import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf
from tensorflow.keras import datasets, layers, models
import os
from datetime import datetime


tf.config.experimental.set_memory_growth(tf.config.experimental.list_physical_devices('GPU')[0],True)
fs      = 16000
size    = 512  # Size of each frame: 32 ms 512
step    = 242  # Size of step for each frame with 100*(1-step/size) % overlap.
numb    = (fs-size+step)//step  # Number of frames. 242 is the closest to 50% overlap with whole number frames.
numBins = 26
Ts = 10
n = 1.2
user = os.getlogin()

kw = os.listdir(path = f"/home/{user}/Desktop/KWS/Audio/TrainingSet")

words = np.empty([len(kw), numBins, numb])
for i, word in enumerate(kw):
    if i == 0:
        Words = np.load(f"/home/{user}/Desktop/KWS/MFCCs/TrainingSet/{word}MFCCVector.npy")
        WordsT = np.load(f"/home/{user}/Desktop/KWS/MFCCs/TestingSet/{word}MFCCVector.npy")
        Label = np.repeat(i, len(Words))
        LabelT = np.repeat(i, len(WordsT))
    else:
        x = np.load(f"/home/{user}/Desktop/KWS/MFCCs/TrainingSet/{word}MFCCVector.npy")
        y = np.load(f"/home/{user}/Desktop/KWS/MFCCs/TestingSet/{word}MFCCVector.npy")
        Words = np.concatenate((Words, x))
        WordsT = np.concatenate((WordsT, y))
        Label = np.concatenate((Label, np.repeat(i,len(x))))
        LabelT = np.concatenate((LabelT, np.repeat(i,len(y))))
TrainingSet = Words
TestingSet = WordsT

TrainingSet = TrainingSet[:,1:13,:]
TestingSet = TestingSet[:,1:13,:]
TrainingSet = np.expand_dims(TrainingSet,3)
TestingSet = np.expand_dims(TestingSet,3)


#Normalize between 0 and 1
nTrainingSet = (TrainingSet - n*np.amin(TrainingSet))/(n*np.amax(TrainingSet) - n*np.amin(TrainingSet))
nTestingSet = (TestingSet - n*np.amin(TrainingSet))/(n*np.amax(TrainingSet) - n*np.amin(TrainingSet))


model = models.Sequential()
model.add(layers.TimeDistributed(layers.Conv1D(4, (3), strides=(1), activation='relu', padding='same'), input_shape=(12, 65, 1)))
model.add(layers.TimeDistributed(layers.MaxPooling1D()))
model.add(layers.TimeDistributed(layers.Conv1D(32, (3), strides=(1), activation='relu', padding='same')))
model.add(layers.TimeDistributed(layers.MaxPooling1D()))
model.add(layers.TimeDistributed(layers.Conv1D(64, (3), strides=(1), activation='relu', padding='same')))
model.add(layers.TimeDistributed(layers.MaxPooling1D()))



model.add(layers.Flatten())
model.add(layers.Dense(64, activation='relu'))
model.add(layers.Dense(7))


model.summary()


model.compile(optimizer='adam',
              loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
              metrics=['accuracy'])
stopearly = tf.keras.callbacks.EarlyStopping(
    monitor='val_loss', min_delta=0, patience=5, verbose=0, mode='auto',
    baseline=None, restore_best_weights=True
)

history = model.fit(nTrainingSet, Label, epochs=20, validation_data=(nTestingSet, LabelT), batch_size = 32, callbacks = [stopearly])

plt.plot(history.history['accuracy'], label='accuracy')
plt.plot(history.history['val_accuracy'], label = 'val_accuracy')
plt.xlabel('Epoch')
plt.ylabel('Accuracy')
plt.ylim([0.5, 1])
plt.legend(loc='lower right')

test_loss, test_acc = model.evaluate(nTestingSet,  LabelT, verbose=2)
print(test_acc, "loss: ", test_loss)

print(LabelT)

model.save(f"/home/{user}/Desktop/KWS/Model/model.h5")
np.save(f"/home/{user}/Desktop/KWS/Model/model_minval", np.amin(TrainingSet))
np.save(f"/home/{user}/Desktop/KWS/Model/model_maxval", np.amax(TrainingSet))
np.save(f"/home/{user}/Desktop/KWS/Model/model_kw", kw)

predictions = model.predict_classes(nTestingSet, batch_size = (nTestingSet).size)
conf = tf.math.confusion_matrix(LabelT, predictions,)

print(conf)
