# Only works for Linux
# Program used to record the 1 second audio files for the training set.
# The files will be saved in Documents/Audio

import os
from datetime import datetime
from tkinter import *
from tkinter import ttk
import sounddevice as sd
import scipy.io.wavfile as wavy
from scipy import signal as sig

REC = []
glint = 0
duration = 1    # seconds
delay = 0.2     # seconds
FS = 48000      # Recording frequency
fs = 16000      # Downsampled frequency
user = os.getlogin()
phrases = ["Telebot","Record", "Stop", "Save", "Discard", "Replay"]
for i in phrases:
    os.makedirs(f"/home/{user}/Desktop/KWS/Audio/Original/{i}",exist_ok=True)
    os.makedirs(f"/home/{user}/Desktop/KWS/Audio/Downsampled/{i}",exist_ok=True)

see = os.popen("pacmd list-sources | grep '* index: '| cut -d ':' -f2").read()
os.system(f"pacmd set-source-volume {int(see)} 16384")

def switchButtonState(status):
    if status == 0:
        plyBut['state'] = kepBut['state']= dscBut['state']=DISABLED
        recBut['state'] = ACTIVE
    elif status == 1:
        recBut['state'] = DISABLED
        plyBut['state'] = kepBut['state']= dscBut['state']=ACTIVE


def keywordDisplay():
    global glint
    global phrases
    if glint in range(0,5):
        glint = glint+1
    else: glint = 0
    keyBut.config(text=phrases[glint])

def record(*args):
    print("Recording")
    RECORD = sd.rec(int((duration+delay) * FS), samplerate=FS, channels=1) #  consider setting latency = ('low', 'low') for realtime
    sd.wait()
    RECORD = RECORD[int(delay*FS):]
    global REC
    REC = RECORD
    switchButtonState(1)
    msg.config(text="Stopped recording")



def play(*args):
    sd.play(REC, FS)
    sd.wait()


def keep(keyword):
    user = os.getlogin()
    audID = user+"__"+keyword+"__"+datetime.now().strftime("%d_%m_%Y__%H_%M_%S")
    rec = sig.resample(REC, fs*duration)
    wavy.write(f"/home/{user}/Desktop/KWS/Audio/Original/{keyword}/{audID}", FS, REC)
    wavy.write(f"/home/{user}/Desktop/KWS/Audio/Downsampled/{keyword}/d_{audID}", fs, rec)
    switchButtonState(0)
    msg.config(text="Saved:   "+ audID+".wav")
    keywordDisplay()

def discard(*args):
    REC = []
    switchButtonState(0)


def calculate(*args):
    try:
        value = float(feet.get())
        meters.set((0.3048 * value * 10000.0 + 0.5)/10000.0)
    except ValueError:
        pass

root = Tk()
root.title("Recorder")
mainframe = ttk.Frame(root, padding="4 4 12 12")
mainframe.grid(column=0, row=0, sticky=(N, W, E, S))
root.columnconfigure(0, weight=1)
root.rowconfigure(0, weight=1)

ttk.Label(mainframe, text="Keyword:").grid(column=1, row=1, sticky=W)

keyBut = ttk.Label(mainframe, text ="Telebot")
keyBut.grid(column=2, row=1, sticky=W)

plyBut = ttk.Button(mainframe, text="Play", command=play, state =DISABLED)
plyBut.grid(column=2, row=2, sticky=W)

recBut = ttk.Button(mainframe, text="Record", command=record, state =ACTIVE)
recBut.grid(column=3, row=2, sticky=W)


kepBut = ttk.Button(mainframe, text="Keep", command= lambda: keep(phrases[glint]), state =DISABLED)
kepBut.grid(column=1, row=3, sticky=W)

dscBut = ttk.Button(mainframe, text="Discard", command=discard, state =DISABLED)
dscBut.grid(column=4, row=3, sticky=W)

msg = ttk.Label(mainframe, text ="")
#msg.grid(column=1, row=4, sticky=W)
msg.grid(column=1, row=4, padx=10, pady=10, sticky="W", columnspan=4, rowspan=4)

for child in mainframe.winfo_children(): child.grid_configure(padx=5, pady=5)

root.mainloop()
