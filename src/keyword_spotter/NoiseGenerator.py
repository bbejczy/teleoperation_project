import sounddevice as sd
import scipy.io.wavfile as wavy
import numpy as np
from scipy import signal as sig
import os
from datetime import datetime
user = os.getlogin()
datime = datetime.now().strftime("%d_%m_%Y__%H_%M_%S")
duration = 900  # seconds
FS = 48000      # Origianl frequency
fs = 16000      # Downsampled frequency

print("Recording")
REC = sd.rec(int(duration * FS), samplerate=FS, channels=1)[:,0]
sd.wait()
print("Stoppped recording")

rec = sig.resample(REC, fs*duration)
step = 8000
for n, i in enumerate(range(0, duration*fs, step)):
    if((i+fs) <= duration*fs):
        wavy.write(f"/home/{user}/Desktop/KWS/Audio/Original/Noise/Noise_{n}_{datime}", fs, rec[i:fs+i])
        wavy.write(f"/home/{user}/Desktop/KWS/Audio/Downsampled/Noise/d_Noise_{n}_{datime}", fs, rec[i:fs+i])
