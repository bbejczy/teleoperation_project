import sys
import sounddevice as sd
import numpy as np  # Make sure NumPy is loaded before it is used in the callback
assert np  # avoid "imported but unused" message (W0611)
import matplotlib.pyplot as plt
import numpy as np
from scipy import signal as sig
from scipy.fft import dct
import time
import scipy.io.wavfile as wavy
import tensorflow as tf
from tensorflow.keras import datasets, layers, models

tf.config.experimental.set_memory_growth(tf.config.experimental.list_physical_devices('GPU')[0],True)
print("Num GPUs Available: ", len(tf.config.experimental.list_physical_devices('GPU')))

duration= 1         # seconds
FS      = 48000     # Original frequency
fs      = 16000     # Downsampled frequency
size    = 512       # Size of each frame: 32 ms 512
step    = 242       # Size of step for each frame with 100*(1-step/size) % overlap.
numb    = (fs-size+step)//step  # Number of frames. 512-242=270 is the closest to 50% overlap with whole number frames.
numBins = 26
lwHz    = 80
hiHz    = 7600
empty   = np.empty((FS), dtype=np.float32)

i       = 0

ncoeff  = 1.2
min     = np.load(f"/home/{user}/Desktop/KWS/Model/model_minval.npy")
max     = np.load(f"/home/{user}/Desktop/KWS/Model/model_maxval.npy")

kw      = np.load(f"/home/{user}/Desktop/KWS/Model/model_kw.npy")
major   = np.repeat(kw.index("Noise"), 15)

# Load pre-trained CNN model
model = tf.keras.models.load_model("/home/rohboz/Documents/Bachelor/Code/Model/modelnew_089.h5")
model.summary()


def callback(indata, frames, time, status):
    global i
    i = i+1
    global empty
    global major
    if i < 30:
        empty = np.concatenate((empty,indata[:,0]))[indata.size:]
    else :
        empty = np.concatenate((empty,indata[:,0]))[indata.size:]
        livemfcc = mfcc(empty)
        predClass = CNN(livemfcc)
        major = np.concatenate(([predClass], major))[:major.size]
        majorpredClass = np.bincount(major).argmax()
        print(kw[majorpredClass])


def mfcc(REC):
    rec = sig.resample(REC, fs*duration)

    hamFrames = np.empty([numb, size])
    frqFrames = np.empty([numb, size],dtype = np.cfloat)
    pwrFrames = np.empty([numb, size])

    hamming = (25/46)-(21/46)*np.cos(2*np.pi*np.arange(0,size)/(size-1))
    for n, i in enumerate(range(size, fs+1, step)):
        hamFrames[n] = np.multiply(rec[(i-size):i],hamming)
        frqFrames[n] = np.fft.fft(hamFrames[n], size)

    #1.2# Power spectral estimate of each frame
    pwrFrames = (np.abs(frqFrames[:,0:size//2 + 1])**2)/(size//2+1)

    melRange = np.linspace(1127*np.log(1+lwHz/700) , 1127*np.log(1+hiHz/700), numBins+2)
    hzRange = 700 * (np.exp(melRange/1127) - 1)
    hzRange = np.around((size/2+1) * hzRange / (fs/2))
    fltBank = np.zeros((numBins, size//2+1))

    # Compute mel-scaled filter bank
    for i in range(0,numBins):
        for j in range(int(hzRange[i]), int(hzRange[i+1])):
            fltBank[i,j] = (j - hzRange[i]) / (hzRange[i+1]-hzRange[i])
        for j in range(int(hzRange[i+1]), int(hzRange[i+2])):
                fltBank[i,j] = (hzRange[i+2]-j) / (hzRange[i+2]-hzRange[i+1])

    #Apply the mel filterbank to the power spectra, sum the energy in each filter.
    pwrBank = np.matmul(fltBank, pwrFrames.T)

    #Take the logarithm of all filterbank energies.
    logBank = np.log(pwrBank)

    # #Take the DCT of the log filterbank energies.
    # #Keep DCT coefficients 2-13, discard the rest.
    mfcc = dct(logBank, axis=0, norm="ortho")[1:13,:]
    return mfcc

def CNN(MFCCs):
    predInput = np.expand_dims(np.expand_dims(MFCCs, axis=2), axis=0)
    npredInput = (predInput - ncoeff*min)/(ncoeff*max - ncoeff*min)
    prediction = model.predict_classes(npredInput)
    return(int(prediction))

try:
     with sd.InputStream(samplerate=48000, blocksize=2048, channels=1, latency = 0.1, callback=callback):
         print("Start")
         CNN(np.load("/home/rohboz/Documents/Python/Voice/Audio/Downsampled/OneMFCCVector.npy"))
         input()
except KeyboardInterrupt:
    sys.exit()
