import scipy.io.wavfile as wavy
import numpy as np
from scipy.fft import dct
import time
import os

user = os.getlogin()
kw = os.listdir(path = f"/home/{user}/Desktop/KWS/Audio/TestingSet")
print(kw)
for word in kw:
    list = os.listdir(path = f"/home/{user}/Desktop/KWS/Audio/TestingSet/{word}")
    mfcc = np.empty([len(list), 26, 65])
    for count, track in enumerate(list):
        fs, rec  = wavy.read(f"/home/{user}/Desktop/KWS/Audio/TestingSet/{word}/{track}")
        if fs == rec.size:
            duration = 1    # seconds
            FS = 48000      # Original frequency
            fs = 16000      # Downsampled frequency

            # #Set frame dimension for the signal into 122 even frames of 32 ms (512) with 25% (8 ms or 128) overlap.
            size    = 512  # Size of each frame: 32 ms 512
            step    = 242  # Size of step for each frame with 100*(1-step/size) % overlap. 270 sample overlap
            numb    = (fs-size+step)//step  # Number of frames. 242 is the closest to 50% overlap with whole number frames.
            numBins = 26 # number of filters in mel filterbank
            lwHz    = 80
            hiHz    = 7600

            # Initialize empy containers for the preprocessing
            hamFrames = np.empty([numb, size])
            frqFrames = np.empty([numb, size],dtype = np.cfloat)
            pwrFrames = np.empty([numb, size])

            # #1# For each frame calculate the periodogram estimate of the power spectrum.
            # #1.1# DFT with Hamming-window for each frame
            hamming = (25/46)-(21/46)*np.cos(2*np.pi*np.arange(0,size)/(size-1))
            for n, i in enumerate(range(size, fs+1, step)):
                hamFrames[n] = np.multiply(rec[(i-size):i],hamming)
                frqFrames[n] = np.fft.fft(hamFrames[n], size)

            #1.2# Power spectral estimate of each frambe
            pwrFrames = (np.abs(frqFrames[:,0:size//2 + 1])**2)/(size//2+1)

            melRange = np.linspace(1127*np.log(1+lwHz/700) , 1127*np.log(1+hiHz/700), numBins+2)
            hzRange = 700 * (np.exp(melRange/1127) - 1)
            hzRange = np.around((size/2+1) * hzRange / (fs/2))
            fltBank = np.zeros((numBins, size//2+1))

            # Compute mel-scaled filter bank
            for i in range(0,numBins):
                for j in range(int(hzRange[i]), int(hzRange[i+1])):
                    fltBank[i,j] = (j - hzRange[i]) / (hzRange[i+1]-hzRange[i])
                for j in range(int(hzRange[i+1]), int(hzRange[i+2])):
                        fltBank[i,j] = (hzRange[i+2]-j) / (hzRange[i+2]-hzRange[i+1])

            #Apply the mel filterbank to the power spectra, sum the energy in each filter.
            pwrBank = np.matmul(fltBank, pwrFrames.T)

            #Take the logarithm of all filterbank energies.
            logBank = np.log(pwrBank)

            # #Take the DCT of the log filterbank energies.
            mfcc[count] = dct(logBank, axis=0, norm="ortho")

        else: print("not ok: ", track)
    print(word," ", count, "", mfcc.shape)
    print(mfcc)
    np.save(f"/home/{user}/Desktop/KWS/MFCCs/TestingSet/{word}MFCCVector", mfcc)
