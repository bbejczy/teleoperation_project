//ros
#include "ros/ros.h"
#include "sensor_msgs/Joy.h"
//eigen
#include <iostream>
#include <eigen3/Eigen/Dense>
#include <eigen3/Eigen/Geometry>
#include <cstdio>
#include <ctime>

using namespace Eigen;
using namespace std;

int counter1 = 1;
int counter2 = 1;
int pMode = 0;

float posInputX, posInputY, posInputZ, posInputX0, posInputY0, posInputZ0; // consecutive frame pos values
float posDiffX, posDiffY, posDiffZ; // position difference between the frames
float stateX, stateY, stateZ; // EEF actual position
float posDesiredX = 0; // desired EEF position, which is sent to MATLAB
float posDesiredY = 0;
float posDesiredZ = 0;

int buttonInput0, buttonInput1, buttonInput1p, buttonInput2; // buuton0 - dead man's switch, button1 - precision level, button2 - gripper (gets passed send to gripper code)
int buttonInput02; // placeholder

float beta = 0.6; // smoothing filter ratio
float pi = 3.14159265359;

float posPrecisionLevel = 1; // initial pos scaling factor
float oriPrecisionLevel = 1; // initial ori scaling factor
float posPrecisionLevelZ = 1; // initial scaling factor for Z moiton
int settingID = 0;
int stationID = 0;

bool changed;

sensor_msgs::Joy joy;
ros::Publisher  pub_joy;


void joyCallback(const sensor_msgs::Joy::ConstPtr& msg)
{
   //cout << "joyCallback enter No #" << counter1 << endl;
   counter1++;

   buttonInput0 = msg->buttons[0]; // rethink the naming for msg (and for joy_msgs)
   buttonInput1 = msg->buttons[1];
   if(msg->buttons[1] == 0 && buttonInput1p == 1)
   {
     buttonInput1 = 1;
   }
   buttonInput1p = msg->buttons[1];

   buttonInput2 = msg->buttons[2];

   settingID = msg->buttons[3];
   stationID = msg->buttons[4];

   posInputX = -msg->axes[0];
   posInputY = -msg->axes[2];
   posInputZ = -msg->axes[1];

}

void joy2Callback(const sensor_msgs::Joy::ConstPtr& msg)
{
   cout << "joy2Callback enter No #" << counter2 << endl;
   counter2++;
   stateX = msg->axes[0];
   stateY = msg->axes[1];
   stateZ = msg->axes[2];

   buttonInput02 = msg->buttons[0];
}

void ScalingFunc()
{

  switch(pMode)
  {
  case 0:
    posPrecisionLevel = 0.5;
    posPrecisionLevelZ = posPrecisionLevel;
    pMode += buttonInput1;
    break;
  case 1:
    posPrecisionLevel = 0.2;
    posPrecisionLevelZ = posPrecisionLevel;
    pMode += buttonInput1;
    break;
  case 2:
    posPrecisionLevel = 0;
    posPrecisionLevelZ = 0.2;
    pMode += buttonInput1;
    break;
  case 3:
    posPrecisionLevel = 1;
    posPrecisionLevelZ = posPrecisionLevel;
    pMode = 0;
    break;
  }
  //std::this_thread::sleep_for (std::chrono::milliseconds(500)); // prevent from entering multiple times upon a button click
}

void SettingFunc()
{
  switch(settingID)
  {
     case 0 :
        oriPrecisionLevel = posPrecisionLevel;
        std::cout << "case 0 \r\n";
        break;
     case 1 :
        oriPrecisionLevel = 0; // make a new var to send to Unity
        std::cout << "case 1 \r\n";
        break;
     default :
        std::cout << "Invalid case num \r\n";
        break;
  }
}

void TrajectoryGen() // control is initiated
{
  //first = true; // reset

  std::cout  << "DM switch \r\n";

  if(changed)
    {
      posDiffX = 0;
      posDiffY = 0;
      posDiffZ = 0;
      changed = false;
    }

  else
    {
      posDiffX = posPrecisionLevel*beta*(posInputX0 - posInputX)+(1-beta)*posDiffX; // fining the pos difference between the frames, applying smoothing and scaling
      posDiffY = posPrecisionLevel*beta*(posInputY0 - posInputY)+(1-beta)*posDiffY;
      posDiffZ = posPrecisionLevelZ*beta*(posInputZ0 - posInputZ)+(1-beta)*posDiffZ;
    }

  posDesiredX += posDiffX; // accumulative difference
  posDesiredY += posDiffY;
  posDesiredZ += posDiffZ;

  posInputX0 = posInputX; // current frame becomes the previous frame
  posInputY0 = posInputY;
  posInputZ0 = posInputZ;

  // joy_msgs.axes[0] = posDesiredX; // values are sent
   joy.axes[1] = posDesiredY;
  // joy_msgs.axes[2] = posDesiredZ;

  std::cout << "DesX: " << posDesiredX << "\r\n" << "DesY: " << posDesiredY << "\r\n" << "DesZ: " << posDesiredZ  << "\r\n";

}

void DeadMansSwitch() // dead man's switch in triggered
{
  std::cout  << "DM switch \r\n";

  // joy_msgs.axes[0] = stateX; // if dead man's switch is triggered, state is sent as a comand for the robot to go to
  // joy_msgs.axes[1] = stateY;
  // joy_msgs.axes[2] = stateZ;

  posDesiredX = stateX; // if dead man's switch is released, increments are continued to add to the last sent value
  posDesiredY = stateY;
  posDesiredZ = stateZ;

  changed = true; // prevent sudden difference

  std::cout << "stateX: " << stateX << "\r\n" << "stateY: " << stateY << "\r\n" << "stateZ: " << stateZ  << "\r\n";
}

void publish()
{

    joy.axes.resize(7);
    joy.buttons.resize(2); // button inputs

    joy.buttons[0] = 1; // butotn input 1

    //cout << "publish " <<  endl;
    pub_joy.publish(joy);
    cout << pMode << endl;
}



int main(int argc, char **argv)
{
  ros::init(argc, argv, "listener");

    ros::NodeHandle n;
    ros::Subscriber sub = n.subscribe("/controllerPose", 1, joyCallback);
    ros::Subscriber sub2 = n.subscribe("/teleop/joy2", 1, joy2Callback);
    pub_joy = n.advertise<sensor_msgs::Joy>("sub", 1000);

    ros::Rate loop_rate(200); // Hz
    while (ros::ok())
    {

      ScalingFunc();
      SettingFunc();

      if(buttonInput0 == 0)
      DeadMansSwitch();
      else if (buttonInput0 == 1)
      TrajectoryGen();

      publish();

      ros::spinOnce();

      loop_rate.sleep();
    }

  return 0;
}
