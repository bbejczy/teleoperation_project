#include "geometry_msgs/TwistStamped.h"
#include "control_msgs/JointJog.h"
#include "ros/ros.h"
#include "sensor_msgs/Joy.h"
#include "chrono"
#include "thread"
#include "cmath"
#include <iomanip>

// Defining position related variables
float posInputX, posInputY, posInputZ, posInputX0, posInputY0, posInputZ0; // consecutive frame pos values
float posDiffX, posDiffY, posDiffZ; // position difference between the frames
float stateX, stateY, stateZ; // EEF actual position
float posDesiredX = 0.4; // desired EEF position, which is sent to MATLAB
float posDesiredY = 0.2;
float posDesiredZ = 0.6;

// Declaring orientation related variables
float quatInputs1, quatInputv1, quatInputv2, quatInputv3, quatInputs10, quatInputv10, quatInputv20, quatInputv30; // consecutive frame orientation values
float stateQs1, stateQv1, stateQv2, stateQv3; // actual EEF orientation

// Declaring button related variables
int buttonInput0, buttonInput1, buttonInput2; // buuton0 - dead man's switch, button1 - precision level, button2 - gripper (gets passed send to gripper code)

// Defining constants
float beta = 0.6; // smoothing filter ratio
float pi = 3.14159265359;

// Defining home position for the robot to go to before initiating the control
float homeX = 0.4;
float homeY = 0.2;
float homeZ = 0.6;

// Defining temporary values
int pMode = 0; // initial case of precision mode
float posPrecisionLevel = 1; // initial pos scaling factor
float oriPrecisionLevel = 1; // initial ori scaling factor
float posPrecisionLevelZ = 1; // initial scaling factor for Z moiton
int settingID = 0;
int stationID = 0;

// Defining booleans for logic operations
bool changed = false;  // to prevent undesired diferences when initiating control after dead man's switch
bool first = true; // first set of values receveid from MATLAB will be set as the desired robot pos
bool initial = true; // before receiving any values from MATLAB robot is comanded to go to home pos


namespace moveit_jog_arm
{
static const int NUM_SPINNERS = 1;
static const int QUEUE_LENGTH = 1;

class SpaceNavToTwist // artifact naming of the code used for a framework
{
public:
  SpaceNavToTwist() : spinner_(NUM_SPINNERS)
  {
    joy_sub_ = n_.subscribe("joy", QUEUE_LENGTH, &SpaceNavToTwist::joyCallback, this); // controller values
    state_sub_ = n_.subscribe("robot_state", QUEUE_LENGTH, &SpaceNavToTwist::stateCallback, this); // EEF position
    twist_pub_ = n_.advertise<sensor_msgs::Joy>("jog_cmds", QUEUE_LENGTH); // publisher

    spinner_.start();
    ros::waitForShutdown();
  };

  void stateCallback(const sensor_msgs::Joy::ConstPtr& msg) // callback function for EEF position
  {

    if(isnan((float)msg->axes[0]) || msg->axes[0] == 0 ) // preventing 'nan' values before MATLAB starts publishing
    {
      std::cout << "not a number " << "\r\n";
      stateX = homeX;
      stateY = homeY;
      stateZ = homeZ;

    }

    else
    {

      if(first) // first set of values after triggering the dead man's switch
      {
        stateX = msg->axes[0];
        stateY = msg->axes[1];
        stateZ = msg->axes[2];

        stateQs1 = msg->axes[3];
        stateQv1 = msg->axes[4];
        stateQv2 = msg->axes[5];
        stateQv3 = msg->axes[6];
        first = false;
      }

    }

  }

  void joyCallback(const sensor_msgs::Joy::ConstPtr& msg) // callback function for controller values, (currently hosting all the calculations)
  {

    sensor_msgs::Joy joy_msgs;
    joy_msgs.axes.resize(13); // published array size
    joy_msgs.buttons.resize(3);

    buttonInput0 = msg->buttons[0]; // rethink the naming for msg (and for joy_msgs)
    buttonInput1 = msg->buttons[1];
    buttonInput2 = msg->buttons[2];

    settingID = msg->buttons[3];
    stationID = msg->buttons[4];

    posInputX = -msg->axes[0];
    posInputY = -msg->axes[2];
    posInputZ = -msg->axes[1];

    quatInputs1 = msg->axes[3];
    quatInputv1 = msg->axes[6];
    quatInputv2 = msg->axes[4];
    quatInputv3 = msg->axes[5];


    if (buttonInput1 == 1) // cases for choosing between motion scaling factor
    {
      if (pMode == 0)
      {
        posPrecisionLevel = 0.5;
        posPrecisionLevelZ = posPrecisionLevel;
        pMode++;
      }
      else if (pMode == 1)
      {
        posPrecisionLevel = 0.2;
        posPrecisionLevelZ = posPrecisionLevel;
        pMode++;
      }
      else if (pMode == 2)
      {
        posPrecisionLevel = 0;
        posPrecisionLevelZ = 0.2;
        pMode++;
      }
      else if (pMode == 3)
      {
        posPrecisionLevel = 1;
        posPrecisionLevelZ = posPrecisionLevel;
        pMode = 0;
      }
      std::this_thread::sleep_for (std::chrono::milliseconds(500)); // prevent from entering multiple times upon a button click
    }

    // if(stationID == 1)
    //   settingID = 1;
    // else if(stationID == 2)
    //   settingID = 2;

    switch(settingID)
    {
       case 2 :
          oriPrecisionLevel = posPrecisionLevel;
          std::cout << "case 1 \r\n";
          break;
       case 1 :
          oriPrecisionLevel = 0; // make a new var to send to Unity
          std::cout << "case 2 \r\n";
          break;
       default :
          std::cout << "Invalid case num \r\n";
    }


    if (buttonInput0 == 0 ) // dead man's switch in triggered
    {
      std::cout  << "DM switch \r\n";
          if(initial) // home position, before starting to control the robot. (not working currently)
          {
            stateX = homeX;
            stateY = homeY;
            stateZ = homeZ;

            initial = false;
          }

      joy_msgs.axes[0] = stateX; // if dead man's switch is triggered, state is sent as a comand for the robot to go to
      joy_msgs.axes[1] = stateY;
      joy_msgs.axes[2] = stateZ;


      posDesiredX = stateX; // if dead man's switch is released, increments are continued to add to the last sent value
      posDesiredY = stateY;
      posDesiredZ = stateZ;

      changed = true; // prevent sudden difference
    }


    else if (buttonInput0 == 1) // control is initiated
    {
      first = true; // reset

      if(changed)
        {
          posDiffX = 0;
          posDiffY = 0;
          posDiffZ = 0;
          changed = false;
        }

      else
        {
          posDiffX = posPrecisionLevel*beta*(posInputX0 - posInputX)+(1-beta)*posDiffX; // fining the pos difference between the frames, applying smoothing and scaling
          posDiffY = posPrecisionLevel*beta*(posInputY0 - posInputY)+(1-beta)*posDiffY;
          posDiffZ = posPrecisionLevelZ*beta*(posInputZ0 - posInputZ)+(1-beta)*posDiffZ;
        }

      posDesiredX += posDiffX; // accumulative difference
      posDesiredY += posDiffY;
      posDesiredZ += posDiffZ;

      posInputX0 = posInputX; // current frame becomes the previous frame
      posInputY0 = posInputY;
      posInputZ0 = posInputZ;

      joy_msgs.axes[0] = posDesiredX; // values are sent
      joy_msgs.axes[1] = posDesiredY;
      joy_msgs.axes[2] = posDesiredZ;

    }


    joy_msgs.axes[3] = quatInputs10; // previous quaternion values are passed to MATLAB
    joy_msgs.axes[4] = quatInputv10;
    joy_msgs.axes[5] = quatInputv20;
    joy_msgs.axes[6] = quatInputv30;
    joy_msgs.axes[7] = quatInputs1; // current quaternion values are passed to MATLAB
    joy_msgs.axes[8] = quatInputv1;
    joy_msgs.axes[9] = quatInputv2;
    joy_msgs.axes[10] = quatInputv3;
    joy_msgs.axes[11] = posPrecisionLevel; // scaling factor for scaling the orientaion sent to MATLAB
    joy_msgs.axes[12] = oriPrecisionLevel;
    joy_msgs.axes[13] = settingID;

    joy_msgs.buttons[0] = buttonInput0; // dead man's switch is applied for orientation in MATLAB
    joy_msgs.buttons[2] = buttonInput2; // gripper button

    quatInputs10 = quatInputs1; // current quaternion becomes the previous quaternion
    quatInputv10 = quatInputv1;
    quatInputv20 = quatInputv2;
    quatInputv30 = quatInputv3;

    // printing desired EEF position, quaternion difference (-) [just to indicate change], scaling factor
    std::cout << "posX: " << joy_msgs.axes[0] << "\r\n" << "posY: " << joy_msgs.axes[1] << "\r\n" << "posZ: " << joy_msgs.axes[2]  << "\r\n";
    std::cout << "qs1: " << joy_msgs.axes[3]-joy_msgs.axes[7] << "\r\n" << "qv1: " << joy_msgs.axes[4]-joy_msgs.axes[8] << "\r\n" << "qv2: " << joy_msgs.axes[5]-joy_msgs.axes[9]  << "\r\n"<< "qv3: " << joy_msgs.axes[6]-joy_msgs.axes[10]  << "\r\n";
    std::cout << "---------" << posPrecisionLevel << "----------" << "\r\n";

    twist_pub_.publish(joy_msgs); // publishing the two arrays
  }

  ros::NodeHandle n_;
  ros::Subscriber joy_sub_;
  ros::Subscriber state_sub_;
  ros::Publisher twist_pub_;
  ros::AsyncSpinner spinner_;
};
}

int main(int argc, char** argv)
{


  ros::init(argc, argv, "interpreter");

  moveit_jog_arm::SpaceNavToTwist to_twist;


  return 0;
}
