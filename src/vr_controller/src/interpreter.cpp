//ros
#include "ros/ros.h"
#include "sensor_msgs/Joy.h"
//eigen
#include <iostream>
#include <eigen3/Eigen/Core>
#include <eigen3/Eigen/Dense>
#include <eigen3/Eigen/Geometry>
#include <cstdio>
#include <ctime>

using namespace Eigen;
using namespace std;

int counter1 = 1;
int counter2 = 1;
int counterS = 1;
int pMode = 0;

float posInputX, posInputY, posInputZ, posInputX0, posInputY0, posInputZ0; // consecutive frame pos values
float replayX, replayY, replayZ;
float posDiffX, posDiffY, posDiffZ; // position difference between the frames
float stateX, stateY, stateZ; // EEF actual position
float posDesiredX = 0.5110166668891907; // desired EEF position, which is sent to MATLAB
float posDesiredY = 0.1711900234222412;
float posDesiredZ = 0.519831657409668;

float oriInputS, oriInputX, oriInputY, oriInputZ;
float oM00, oM01, oM02, oM10, oM11, oM12, oM20, oM21, oM22; // orientation matrix entries
float roM00, roM01, roM02, roM10, roM11, roM12, roM20, roM21, roM22;

int buttonInput0, buttonInput1, buttonInput1prev, buttonInput2,buttonInput1x; // buuton0 - dead man's switch, button1 - precision level, button2 - gripper (gets passed send to gripper code)
int buttonInput02; // placeholder

float beta = 1; // smoothing filter ratio
float pi = 3.14159265359;

float posPrecisionLevel = 1; // initial pos scaling factor
float oriPrecisionLevel = 1; // initial ori scaling factor
float posPrecisionLevelZ = 1; // initial scaling factor for Z moiton
int settingID = 0;
int stationID = 0;

bool changed, changedOri, first;
bool reached = false;

int replay = 0; //for semi-auto

sensor_msgs::Joy joy;
ros::Publisher  pub_joy;

Eigen::Vector3d c(0.1, 0.2, 0.3); // has to be normalized
Eigen::Vector3d a = c / sqrt(c.transpose() * c);
Eigen::Vector3d b(0,0,0);

Eigen::Vector3d v0;
Eigen::Quaterniond q0;
Eigen::Quaterniond q1;
Quaterniond qDiff;

MatrixXd oriMatrix = MatrixXd::Identity(3,3); // initial EEF orientation matrix
Eigen::Matrix3d rotmDiff;

bool initialQuat = true;

void joyCallback(const sensor_msgs::Joy::ConstPtr& msg)
{
  // cout << "joyCallback enter No #" << counter1 << endl;
   counter1++;

   buttonInput0 = msg->buttons[0]; // rethink the naming for msg (and for joy_msgs)
   // scaling input is set to one after releasing the button (recognizing the sequence [1,0])
   if(msg->buttons[1] == 0 && buttonInput1prev == 1)
   {
     buttonInput1 = 1;
   }
   else
  {
    buttonInput1 = 0;
  }
  //if (buttonInput1 == 1)
  //{
  //  pMode++;
  //  if (pMode == 4)
  //  {
  //   pMode = 0;
  //  }
  //}
   buttonInput1prev = msg->buttons[1];

   buttonInput2 = msg->buttons[2]; // gripper input

   settingID = msg->buttons[3];
   stationID = msg->buttons[4];

   posInputX = msg->axes[0];
   posInputY = msg->axes[2];
   posInputZ = msg->axes[1];

   oriInputS = msg->axes[3];
   oriInputX = msg->axes[4];
   oriInputY = msg->axes[5];
   oriInputZ = msg->axes[6];

}

void joy2Callback(const sensor_msgs::Joy::ConstPtr& msg)
{
   //cout << "joy2Callback enter No #" << counter2 << endl;
   counter2++;
   stateX = msg->axes[0];
   stateY = msg->axes[1];
   stateZ = msg->axes[2];

   buttonInput02 = msg->buttons[0];
}

void matCallback(const sensor_msgs::Joy::ConstPtr& msg)
{
   replayX = msg->axes[0];
   replayY = msg->axes[1];
   replayZ = msg->axes[2];
   roM00 = msg->axes[3];
   roM01 = msg->axes[4];
   roM02 = msg->axes[5];
   roM10 = msg->axes[6];
   roM11 = msg->axes[7];
   roM12 = msg->axes[8];
   roM20 = msg->axes[9];
   roM21 = msg->axes[10];
   roM22 = msg->axes[11];
   replay = msg->buttons[0];
}

void scalingFunc()
{

  switch(pMode)
  {
  case 0:
    cout << "case 0" << endl;
    posPrecisionLevel = 0.5;
    posPrecisionLevelZ = posPrecisionLevel;
    pMode += buttonInput1;
    break;
  case 1:
    cout << "case 1" << endl;
    posPrecisionLevel = 0.2;
    posPrecisionLevelZ = posPrecisionLevel;
    pMode += buttonInput1;
    break;
  case 2:
    cout << "case 2" << endl;
    posPrecisionLevel = 0;
    posPrecisionLevelZ = 0.2;
    pMode += buttonInput1;
    break;
  case 3:
    cout << "case 3" << endl;
    posPrecisionLevel = 1;
    posPrecisionLevelZ = posPrecisionLevel;
    pMode += buttonInput1;
    break;
  case 4:
    pMode = 0;
    break;
  }
  //std::this_thread::sleep_for (std::chrono::milliseconds(500)); // prevent from entering multiple times upon a button click
}

void settingFunc()
{
  switch(settingID)
  {
     case 0 :
        oriPrecisionLevel = posPrecisionLevel;
        std::cout << "case 0 \r\n";
        break;
     case 1 :
        oriPrecisionLevel = 0; // make a new var to send to Unity
        std::cout << "case 1 \r\n";
        break;
     default :
        std::cout << "Invalid case num \r\n";
        break;
  }
}

void trajectoryGen() // control is initiated
{
  //first = true; // reset


    std::cout  << "TrajectoryGen \r\n";

  if(changed)
    {
      posDiffX = 0;
      posDiffY = 0;
      posDiffZ = 0;
      changed = false;
    }

  else
    {
      posDiffX = posPrecisionLevel*beta*(posInputX - posInputX0)+(1-beta)*posDiffX; // fining the pos difference between the frames, applying smoothing and scaling
      posDiffY = posPrecisionLevel*beta*(posInputY - posInputY0)+(1-beta)*posDiffY;
      posDiffZ = posPrecisionLevelZ*beta*(posInputZ - posInputZ0)+(1-beta)*posDiffZ;
    }

  posDesiredX += posDiffX; // accumulative difference
  posDesiredY += posDiffY;
  posDesiredZ += posDiffZ;

  posInputX0 = posInputX; // current frame becomes the previous frame
  posInputY0 = posInputY;
  posInputZ0 = posInputZ;

  if (replay == 1)
   {
     posDesiredX = replayX;
     posDesiredY = replayY;
     posDesiredZ = replayZ;

   }

  //first = true;

  //std::cout << "DesX: " << posDesiredX << "\r\n" << "DesY: " << posDesiredY << "\r\n" << "DesZ: " << posDesiredZ  << "\r\n";
}

void angles()
{
   if(changedOri || oriPrecisionLevel == 0) {
changedOri = false;
   q0.w() = oriInputS;
   v0 << oriInputX,oriInputY,oriInputZ;
   q0.vec() = v0;
   q1 = q0;
}

if(!changedOri) {
   q0.w() = oriInputS;
   v0 << oriInputX,oriInputY,oriInputZ;
   q0.vec() = v0;
   if (initialQuat)
   {
     q1 = q0;
     initialQuat = false;
   }

   qDiff = q0.inverse()*q1;
   qDiff.normalize();
   //qDiff.w() = pow(qDiff.w(),oriPrecisionLevel);
   //qDiff.x() = pow(qDiff.x(),oriPrecisionLevel);
   //qDiff.y() = pow(qDiff.y(),oriPrecisionLevel);
   //qDiff.z() = pow(qDiff.z(),oriPrecisionLevel);
   cout << qDiff.w() << "w" << endl;
   cout << qDiff.x() << "x" << endl;
   cout << qDiff.y() << "y" << endl;
   cout << qDiff.z() << "z" << endl;
   cout << oriPrecisionLevel << endl;
   rotmDiff = qDiff.toRotationMatrix();
   oriMatrix = oriMatrix*rotmDiff;

   b = oriMatrix * a; // for verification

   q1 = q0;

   oM00 = oriMatrix(0,0);
   oM01 = oriMatrix(0,1);
   oM02 = oriMatrix(0,2);
   oM10 = oriMatrix(1,0);
   oM11 = oriMatrix(1,1);
   oM12 = oriMatrix(1,2);
   oM20 = oriMatrix(2,0);
   oM21 = oriMatrix(2,1);
   oM22 = oriMatrix(2,2);
   if (replay == 1)
    {
      oM00 = roM00;
      oM01 = roM01;
      oM02 = roM02;
      oM10 = roM10;
      oM11 = roM11;
      oM12 = roM12;
      oM20 = roM20;
      oM21 = roM21;
      oM22 = roM22;
    }
}
 }

void deadMansSwitch() // dead man's switch in triggered
{

    std::cout  << "DM switch \r\n";

  if(first)
  {
    std::cout  << "DM switch FIRST \r\n";
    posDesiredX = stateX; // if dead man's switch is released, increments are continued to add to the last sent value
    posDesiredY = stateY;
    posDesiredZ = stateZ;
    first = false;
  }


  changed = true; // prevent sudden difference
  changedOri = true; // prevent sudden difference

  //std::cout << "stateX: " << stateX << "\r\n" << "stateY: " << stateY << "\r\n" << "stateZ: " << stateZ  << "\r\n";
}

void presetTrajectory()
{
  // while(!reached)
  // {
  //   posDesiredX = trajAX; // desired EEF pose defined by predefined trajectory
  //   posDesiredY = trajAY;
  //   posDesiredZ = trajAZ;
  //   if(state ~= goal)
  //   {
  //     reached = true;
  //   }
  // }

}

void publish()
{

    joy.axes.resize(12);
    joy.buttons.resize(2); // button inputs

    joy.axes[0] = posDesiredX;
    joy.axes[1] = posDesiredY;
    joy.axes[2] = posDesiredZ;

    joy.axes[3] = oM00;
    joy.axes[4] = oM01;
    joy.axes[5] = oM02;
    joy.axes[6] = oM10;
    joy.axes[7] = oM11;
    joy.axes[8] = oM12;
    joy.axes[9] = oM20;
    joy.axes[10] = oM21;
    joy.axes[11] = oM22;

    joy.buttons[0] = 1; // butotn input 1

    //cout << "publish " <<  endl;
    pub_joy.publish(joy);
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "listener");

    ros::NodeHandle n;
    ros::Subscriber sub = n.subscribe("/teleop/joy", 1, joyCallback);
    ros::Subscriber sub2 = n.subscribe("/teleop/joy2", 1, joy2Callback);
    ros::Subscriber sub3 = n.subscribe("/newtraj", 1, matCallback);
    pub_joy = n.advertise<sensor_msgs::Joy>("sub", 1000);

    ros::Rate loop_rate(100); // Hz
    while (ros::ok())
    {

      scalingFunc();
      settingFunc();
      angles();

       if(buttonInput0 == 0 && replay == 0)
         deadMansSwitch();
       else if (buttonInput0 == 1  || replay == 1)
         trajectoryGen();
       else if (buttonInput0 == 2) // sets desired position to follow predefined traj. should stay in before goal is reached
         presetTrajectory();

      publish();

      ros::spinOnce();

      loop_rate.sleep();
    }

  return 0;
}
