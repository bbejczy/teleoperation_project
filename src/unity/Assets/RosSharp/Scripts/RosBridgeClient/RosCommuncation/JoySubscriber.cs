﻿/*
© Siemens AG, 2017-2018
Author: Dr. Martin Bischoff (martin.bischoff@siemens.com)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
<http://www.apache.org/licenses/LICENSE-2.0>.
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace RosSharp.RosBridgeClient
{
    public class JoySubscriber : Subscriber<Messages.Sensor.Joy>
    {
        public Text MyText;
        //public GameObject Arrow;
        //public Text JoySeven;
        //public Text JoyEight;
        //public Text JoyNine;
        //public Text JoyTen;
        public JoyButtonWriter[] joyButtonWriters;
        public JoyAxisWriter[] joyAxisWriters;

		protected override void Start()
		{
			base.Start();
		}	
        protected override void ReceiveMessage(Messages.Sensor.Joy joy)
        {
            int I = joyButtonWriters.Length < joy.buttons.Length ? joyButtonWriters.Length : joy.buttons.Length;
            for (int i = 0; i < I; i++)
            {
                if (joyButtonWriters[i] != null)
                {
                    joyButtonWriters[i].Write(joy.buttons[i]);
                }
            }
            I = joyAxisWriters.Length < joy.axes.Length ? joyAxisWriters.Length : joy.axes.Length;
            for (int i = 0; i < I; i++)
            {
                if (joyAxisWriters[i] != null)
                {
                    joyAxisWriters[i].Write(joy.axes[i]);
                }
            }
            MyText.text = joy.axes[11].ToString();
            //JoySeven.text = joy.axes[7].ToString();
            //JoyEight.text = joy.axes[8].ToString();
            //JoyNine.text = joy.axes[9].ToString();
            //JoyTen.text = joy.axes[10].ToString();
            //Arrow.transform.rotation = Quaternion.AngleAxis(30, new Vector3(1, 1, 0));
            //Debug.Log(Quaternion.AngleAxis(((Mathf.Acos(joy.axes[7]) * 360) / Mathf.PI), new Vector3(joy.axes[8], joy.axes[9], joy.axes[10])));
            //Arrow.transform.rotation = Quaternion.AngleAxis(((Mathf.Acos(joy.axes[7]) * 360) / Mathf.PI), new Vector3(joy.axes[8], joy.axes[9], joy.axes[10]));
        }
    }
}
