﻿/*
© Siemens AG, 2017-2018
Author: Dr. Martin Bischoff (martin.bischoff@siemens.com)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
<http://www.apache.org/licenses/LICENSE-2.0>.
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.Threading;

namespace RosSharp.RosBridgeClient
{
    public class JoyPublisher : Publisher<Messages.Sensor.Joy>
    {
        private JoyAxisReader[] JoyAxisReaders;
        private JoyButtonReader[] JoyButtonReaders;

        //Button reader for color switch
        public Button A;
        public Button B;

        //Value of Area
        public Text AreaDisplay;
        private string FirstState;

        public string FrameId = "Unity";


        private Messages.Sensor.Joy message;


        protected override void Start()
        {
            base.Start();
            InitializeGameObject();
            InitializeMessage();
            FirstState = "";
        }


        private void Update()
        {
            UpdateMessage();

        }

        private void InitializeGameObject()
        {
            JoyAxisReaders = GetComponents<JoyAxisReader>();
            JoyButtonReaders = GetComponents<JoyButtonReader>();
        }

        private void InitializeMessage()
        {
            message = new Messages.Sensor.Joy();
            message.header.frame_id = FrameId;
            message.axes = new float[7];
            message.buttons = new int[5];

        }

        private void UpdateMessage()
        {

            message.header.Update();

            //message.axes[0] = transform.localEulerAngles.x;
            //message.axes[1] = transform.localEulerAngles.y;
            //message.axes[2] = transform.localEulerAngles.z;

            //message.buttons[0] = (int)transform.localPosition.x;
            //message.buttons[1] = (int)transform.localPosition.y;
            //message.buttons[2] = (int)transform.localPosition.z;

            GameObject righthandpose = GameObject.Find("Right Hand");
            Transform righthandtransform = righthandpose.GetComponent<Transform>();

            message.axes[3] = righthandtransform.transform.rotation.w;
            message.axes[4] = righthandtransform.transform.rotation.x;
            message.axes[5] = righthandtransform.transform.rotation.y;
            message.axes[6] = righthandtransform.transform.rotation.z;

            message.axes[0] = righthandtransform.transform.localPosition.x;
            message.axes[1] = righthandtransform.transform.localPosition.y;
            message.axes[2] = righthandtransform.transform.localPosition.z;



            if (Input.GetKey("joystick button 15") == true)
                message.buttons[0] = 1;
            else
                message.buttons[0] = 0;

            if (Input.GetKey("joystick button 0") == true)
            {
                //Thread.Sleep(200);
                message.buttons[1] = 1;
            }  
            else
                message.buttons[1] = 0;

            if (Input.GetKey("joystick button 5") == true)
            {
                //Thread.Sleep(200);
                message.buttons[2] = 1;
            }
            else
                message.buttons[2] = 0;

            if (A.image.color == Color.green)
            {
                message.buttons[3] = 0;
            }
            else if (B.image.color == Color.green)
            {
                message.buttons[3] = 1;
            }

            if (AreaDisplay.text != FirstState)
            {
                message.buttons[4] = int.Parse(AreaDisplay.text);
                FirstState = AreaDisplay.text;
            }

            //Debug.Log("lock " + Input.GetKey("joystick button 15") + " gripper " + Input.GetKey("joystick button 5") + " mode " + Input.GetKey("joystick button 0"));
            /*            if (Input.anyKey)
                            Debug.Log("A key or mouse click has been detected");*/

            //Debug.Log(" " + message.axes[0] + "   " + message.axes[1] + "   " + message.axes[2] + "   " + message.axes[3] + "   " + message.axes[4] + "   " + message.axes[5] + "   " + message.buttons[0] + "   " + message.buttons[1]);

            /*for (int i = 0; i < JoyButtonReaders.Length; i++)
            {
                message.buttons[i] = (JoyButtonReaders[i].Read() ? 1 : 0);
            }
*/

            Publish(message);
        }

    }
}
