﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LogiScript : MonoBehaviour
{
    private WebCamTexture _webcamTexture;
    private Renderer _renderer;
    // Assign the Material you are using for the web cam feed
    [SerializeField] private Material LogiTex;





    void Start()
    {

        LogiTex = Resources.Load("LogiTex", typeof(Material)) as Material;

        // Grabbing all web cam devices
        WebCamDevice[] devices = WebCamTexture.devices;

        // I just use the first one, use which ever one you need 
        string camName1 = devices[0].name;

        // set the Texture from the cam feed
        WebCamTexture camFeed1 = new WebCamTexture(camName1);

        // Assign the materials texture to the WebCamTexture you made,
        // this applies it to all objects using this Material
        LogiTex.mainTexture = camFeed1;

        // Then start the texture
        camFeed1.Play();

        gameObject.GetComponent<Renderer>().material = LogiTex;

    }
}
