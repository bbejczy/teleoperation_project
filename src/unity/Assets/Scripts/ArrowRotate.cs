﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ArrowRotate : MonoBehaviour
{

    public Text JoySeven;
    public Text JoyEight;
    public Text JoyNine;
    public Text JoyTen;
    Vector3 Hector;
    float Flector;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        Flector = ((Mathf.Acos(float.Parse(JoySeven.text)) * 360) / Mathf.PI);
        Hector = new Vector3(float.Parse(JoyEight.text), float.Parse(JoyNine.text), float.Parse(JoyTen.text));
        transform.rotation = Quaternion.AngleAxis(Flector, Hector);
    }
}