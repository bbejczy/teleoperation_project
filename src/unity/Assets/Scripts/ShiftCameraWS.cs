﻿//Add script as component to rendering plane
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShiftCameraWS : MonoBehaviour
{
    private WebCamTexture _webcamTexture;
    private Renderer _renderer;
    [SerializeField] private Material WebCamTex;

    public Text Station;
    readonly int[,] StationArray = new int[3, 2] { { 0, 2 }, { 1, 0 }, { 2, 1 } };
    string LastFrame;

    //Cam
    WebCamDevice[] devices;
    WebCamTexture camFeed;

    void Shifter(int k)
    {
        camFeed.Stop();
        string camName = devices[k].name;
        camFeed = new WebCamTexture(camName);
        WebCamTex.mainTexture = camFeed;
        camFeed.Play();
        gameObject.GetComponent<Renderer>().material = WebCamTex;
    }
    // Start is called before the first frame update
    void Start()
    {
        WebCamTex = Resources.Load("WebCamTex", typeof(Material)) as Material;
        devices = WebCamTexture.devices;
        string camName = devices[StationArray[int.Parse(Station.text), 1]].name;
        camFeed = new WebCamTexture(camName);
        WebCamTex.mainTexture = camFeed;
        camFeed.Play();
        gameObject.GetComponent<Renderer>().material = WebCamTex;

        LastFrame = Station.text;
    }

    // Update is called once per frame
    void Update()
    {
        if ((int.Parse(Station.text) > 0) && (Station.text != LastFrame))
        {
            Shifter(StationArray[int.Parse(Station.text), 1]);
            LastFrame = Station.text;
        }
        else if ((Station.text.ToString() == "0") && (Station.text != LastFrame))
        {
            Debug.Log("Moving to station");
            LastFrame = "0";
        }
        else if ((Station.text.ToString() == "-1") && (Station.text != LastFrame))
        {
            Debug.Log("I am lost!");
            LastFrame = "-1";
        }
    }
}