﻿// www.medium.com/@danielle.co3tz33/laser-pointer-with-ui-buttons-unity3d-for-vr-f0293022e489
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Threading;
using Valve.VR;
using Valve.VR.Extras;
using Valve.VR.InteractionSystem;

public class LaserPointerScript : MonoBehaviour
{
    public Button A;
    public Button B;

    public Text AreaDisplay;
    public Button Left;
    public Button Right;

    public static GameObject currentObject;
    int currentID;
    int k;
    int Area;
    int t;
    // Start is called before the first frame update
    void Start()
    {
        currentObject = null;
        currentID = 0;
        k = 0;
        Area = 1;
        A.image.color = Color.green;
        B.image.color = Color.white;
    }

    // Update is called once per frame
    void Update()
    {
        //Sends out a Raycast and returns an array filled with everything it hit
        RaycastHit[] hits;
        hits = Physics.RaycastAll(transform.position, transform.forward, 200.0F);
        if(hits.Length == 0)
        {
            k = 0;
            Left.image.color = Color.white;
            Right.image.color = Color.white;
        }
        //Goes through all the hit objects and check if any of them were our button
        for (int i = 0; i < hits.Length; i++)
        {
            RaycastHit hit = hits[i];

            //I use the object ID to determine if I have already run the code for this object
            int id = hit.collider.gameObject.GetInstanceID();

            currentID = id;
            currentObject = hit.collider.gameObject;
            
            //Checks based of the name
            string name = currentObject.name;
            if ((name == "StateFree") && (k<100))
            {
                k++;
                if(k == 100)
                {
                    B.image.color = Color.white;
                    A.image.color = Color.green;
                }
            }
            else if ((name == "StateFunc") && (k < 100))
            {
                k++;
                if (k == 100)
                {
                    A.image.color = Color.white;
                    B.image.color = Color.green;
                }
            }
            else if ((name == "AreaLeft") && (k < 100))
            {
                k++;
                if (k == 100)
                {
                    Left.image.color = Color.green;
                    Area--;
                    AreaDisplay.text = Area.ToString();
                }
            }
            else if ((name == "AreaRight") && (k < 100))
            {
                k++;
                if (k == 100)
                {
                    Right.image.color = Color.green;
                    Area++;
                    AreaDisplay.text = Area.ToString();
                }
            }
        }
    }
}
