﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchView : MonoBehaviour
{
    public GameObject cloud;

    void Update()
    {
        if (Input.GetKeyUp(KeyCode.LeftArrow))
        {
            transform.SetPositionAndRotation(new Vector3(-8, 10, -25), transform.rotation);
            cloud.transform.SetPositionAndRotation(new Vector3(-5, 11, -25.7f), cloud.transform.rotation);
        }
        else if (Input.GetKeyUp(KeyCode.UpArrow))
        {
            transform.SetPositionAndRotation(new Vector3(8, 10, -25), transform.rotation);
            cloud.transform.SetPositionAndRotation(new Vector3(9, 11, -26.7f), cloud.transform.rotation);
        }
        else if (Input.GetKeyUp(KeyCode.DownArrow))
        {
            transform.SetPositionAndRotation(new Vector3(0, 5, -30), transform.rotation);
            cloud.transform.SetPositionAndRotation(new Vector3(3, 7, -30.7f), cloud.transform.rotation);
        }

    }

}
