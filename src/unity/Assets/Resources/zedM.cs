﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using sl;


public class zedM : MonoBehaviour
{
    public Material depthMaterial;
    private Texture2D depthTexture;
    private float[,] depthValues;
    private sl.ZEDCamera zedCam;
    public MeshRenderer meshRenderer;
    void Update()
    {
        meshRenderer.material = new Material(Shader.Find("Standard"));
        depthTexture = zedCam.CreateTextureImageType(sl.VIEW.LEFT) as Texture2D;
        depthMaterial.mainTexture = depthTexture;
        depthTexture.Apply();
        meshRenderer.material.SetTexture("_MainTex", depthTexture);
    }


}



