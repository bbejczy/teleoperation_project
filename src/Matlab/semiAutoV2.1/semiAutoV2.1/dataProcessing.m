function  newTrajectory = dataProcessing(newStartingPose)

load('dataSetObject.mat','demoDataset')

demoNum = length(demoDataset);
thetaThres = 35; %tolerance for deviation
shortestDemo = 10000000;
newStartPose = newStartingPose;
scalar = 2; %larger the scalar the faster the convergence towards the meanPos
velSegmentArray = single.empty; 
axisNumber = 6; %what axis to print
segmentEndID = [];
largeStep = [];
unitScale = 100;
%% pre filtering the zero coloumns

for k = 1:demoNum
    
    c = 0;
    
    loadV1 = cell2mat(demoDataset(1,k));
    
    firstColoumn = [loadV1(1,1);loadV1(2,1);loadV1(3,1);loadV1(4,1);loadV1(5,1);loadV1(6,1);loadV1(7,1);loadV1(8,1)];
    
    loadVClone = loadV1;
    
    loadVClone(:,length(loadVClone)) = [];
    
    loadVClone = [firstColoumn loadVClone];
    
    loadVDiff = loadV1 - loadVClone; %finding the velocities
    
    for i = 1:(length(loadV1))
        
        if loadVDiff(1,i) == 0 && loadVDiff(2,i) == 0 && loadVDiff(3,i) == 0  && loadVDiff(4,i) == 0 && loadVDiff(5,i) == 0 && loadVDiff(6,i) == 0
            
            loadV1(:,i-c) = [];
            c = c+1;
            
        end
        
    end
    
    loadV1(7,:) = single(zeros(1,length(loadV1)));
    
    sequenceN=1;
   
   %refill the sequence number row after elemination
    
   for j = 1:length(loadV1)
       
      loadV1(7,j) = sequenceN;
      
      sequenceN = sequenceN+1;
       
   end
   
   %find the shortest trial
   
    if length(loadV1) < shortestDemo
       
        shortestDemo = length(loadV1);
        
    end
    
    demoDataset{k} = loadV1;
    
end

%% scaling up the values

for k = 1:demoNum
   
    loadV3 = cell2mat(demoDataset(1,k))
    
    for j = 1:6
       
        loadV3(j,:) = loadV3(j,:)*unitScale;
        
    end
    
    demoDataset{k} = loadV3;
    
end

%% finding the ratio and the removeable IDs

for k = 1:demoNum
    
    diffLength = length(demoDataset{k})-shortestDemo;

    ratio = length(demoDataset{k})/diffLength;

    vectorNum = 1:diffLength;

    removeID{k} = round(ratio*vectorNum); %finding the IDs of the removable values
    
    
end

%% removing the entries sequentially

c = 0; 

for i = 1:length(removeID)
    
    remEntry = single(cell2mat(removeID(:,i)));
    trialEntry = cell2mat(demoDataset(:,i));

    for j = 1:length(remEntry)     
        
        for k = 1:length(trialEntry)
            
            
            if remEntry(1, j) == trialEntry(7, (k-c))
               
                trialEntry(:,(k-c)) = [];
                break;
%                 c = c+1;
                                
            end
                          
        end
    end
    
    demoSampled{i} = trialEntry;   
    
end

%% calculating and smoothening velocities

for k = 1:length(demoSampled)
    
%create a clone with one cell shift
trajOrig = cell2mat(demoSampled(1, k));

startingPoint = trajOrig(:,1);

startColoumn = [startingPoint(1,1);startingPoint(2,1);startingPoint(3,1);startingPoint(4,1);startingPoint(5,1);startingPoint(6,1);0;0];

trajClone = trajOrig;
trajClone(:,length(trajClone)) = [];
trajClone = [startColoumn trajClone];

movingAverage = single(zeros(8,shortestDemo));

demoVelocity = trajOrig - trajClone;

    for j = 1:6

        for i = 2:(length(demoVelocity)-1)
            movingAverage(j,i) = (demoVelocity(j,i-1) + demoVelocity(j,i) + demoVelocity(j,i+1))/3;
        end

    end

velocityData{k} = demoVelocity;

end

%% mean trajectories and velocities


sumPose  = single(zeros(8,shortestDemo));
sumVel = single(zeros(8,shortestDemo));

for i = 1:length(demoSampled)
    
    loadV2Pose = cell2mat(demoSampled(1, i));
    loadV2Vel =  cell2mat(velocityData(1, i));
    sumPose = sumPose+loadV2Pose;
    sumVel = sumVel+loadV2Vel;
    meanPose = sumPose/demoNum;
    meanVel = sumVel/demoNum;
    
end


%% comparing the individual demo to the mean velocity, insertion of token in row 8 if it satisfies the threshold

thetaMatrix = [length(velocityData),shortestDemo];
tokenRow = single(zeros(8,shortestDemo));
tokenRow(8,1) = demoNum; %set the first one default accepted
% figure('Name','Demonstration thetas','NumberTitle','off');
for k=1:demoNum
    
    targetDemo = velocityData{k};
    
        for i=1:length(targetDemo)

            meanVector = [meanVel(1,i) meanVel(2,i) meanVel(3,i)];

            demoVector = [targetDemo(1,i) targetDemo(2,i) targetDemo(3,i)];

            theta(i) = acosd((meanVector(1)*demoVector(1)+meanVector(2)*demoVector(2)+meanVector(3)*demoVector(3))/(sqrt((meanVector(1)^2)+(meanVector(2)^2)+(meanVector(3)^2))*sqrt((demoVector(1)^2)+(demoVector(2)^2)+(demoVector(3)^2))));

            thetaMatrix(k,i) = theta(i);

            if theta(i) <= thetaThres %checking if the given sample is under the threshold

                targetDemo(8,i) = 1;

            end

        end
        
    tokenRow = tokenRow(1,:) + targetDemo(8,:);        
    velocityData{k} = targetDemo; % feeding the marking tokens to the velocity dataset 8th row 
    
%     plot(theta)
%     grid on
%     hold on;
    
end
% figure('Name','thetas2','NumberTitle','off');
% plotNum = 1;
% for i = 1:3
%         
%         
%         subplot(3, 1, plotNum);
%         
%         plot(meanVel(i,:));
%         
%     for j=1:shortestDemo
%   
%             if tokenRow(1,j) >= demoNum
%                 plot(j,meanVel(i,j),'r*');
%             end
%         hold on; 
%     end        
%         %cycle between the 6 subplots
%         if plotNum == 3
%             plotNum = 0;
%         end 
%         plotNum= plotNum+1;
% 
% end


%% calculating the end of velocity-control segments

newTrajectory = [newStartPose, single(zeros(8,length(meanPose)-1))]*unitScale;
c = 1;

segmentEnd(:,1) = newTrajectory(:,1); %hardcoding the first point as a segmentEnd
segmentEndID(1) = 1;
segmentID = 2; %starting from second for the above mentioned reason

for i = 2:length(meanPose)
   
    newTrajectory(:,i) = meanPose(:,i);
    
end

%plot for debugging

figure('Name','newTrajectory','NumberTitle','off');

% plotNum = 1;
% 
% for i = 1:axisNumber   
%         
%         subplot(axisNumber, 1, plotNum);
%         plot(newTrajectory(i,:),'g.','LineWidth',3)
%         hold on; 
% 
%         %cycle between the 6 subplots
%         if plotNum == axisNumber
%             plotNum = 0;
%         end 
%         plotNum= plotNum+1;
% 
% end

%% finding the velocity and the general trajectory segments

for i = 2:(length(tokenRow)-1)
   
    if tokenRow(i) == demoNum %if the number of tokens in a coloumn is equal to the demonstration numbers we apply velocity
        
        velSegmentArray(1,c) = i;
        
        c = c+1;
        
        if (tokenRow(i+1) < demoNum)
            
            if(length(velSegmentArray) >= 5) %only 5+ sample velocity segments are considered valid (smaller disregarded)
            
                for k = 1:length(velSegmentArray)
                    
                    index = velSegmentArray(1,k);

                    newTrajectory(1:3,index) = newTrajectory(1:3,index-1) + meanVel(1:3,index);
                    newTrajectory(4:6,index) = newTrajectory(4:6,index-1) + meanVel(4:6,index);
                    
                    
                    %plot for debugging
%                     plotNum = 1;
% 
%                     for j = 1:axisNumber %plotting segment marker
% 
% 
%                             subplot(axisNumber, 1, plotNum);
%                             plot(index,newTrajectory(j,index),'r*')
%                             hold on; 
% 
%                             %cycle between the 6 subplots
%                             if plotNum == axisNumber
%                                 plotNum = 0;
%                             end 
%                             plotNum= plotNum+1;
% 
%                     end
                    %end of plot debugging

                end
                
                segmentEndID(segmentID) = index; %% don't outcomment this
                
                    %plot for debugging
%                     plotNum = 1;
% 
%                     for j = 1:axisNumber %% plotting segment ends
% 
% 
%                             subplot(axisNumber, 1, plotNum);
%                             plot(index,newTrajectory(j,index),'gO')
%                             hold on; 
% 
%                             %cycle between the 6 subplots
%                             if plotNum == axisNumber
%                                 plotNum = 0;
%                             end 
%                             plotNum= plotNum+1;
% 
%                     end
                    %end of plot debugging
                
                
                segmentEnd(:,segmentID) = newTrajectory(:,index);
                segmentID = segmentID+1;
                
                lengthValidSegment = length(velSegmentArray);

                c = 1;
                velSegmentArray = []; %reset segmentarray
                
            else
                
            c = 1;
            velSegmentArray = [];
            
            end
        end
        
    else
        
        newTrajectory(:,i) = meanPose(:,i); % if it is not a velocity segment we proceed with the general trajectory
      
    end
    
    
end

%% finding where do we need convergence
if ~isempty(segmentEndID)
    for k = 1:length(segmentEndID) %COLOUMN

        for i = 1:6 %ROW

            maxVel(i) = max(meanVel(i,:)); %finding the max velocity for each demo's each axis

            segmentEndDiff(i) = newTrajectory(i,segmentEndID(k)+1)-newTrajectory(i,segmentEndID(k)); %finding the step it has to take from the segmentend till the next sample 

            if abs(segmentEndDiff(i)) >= (abs(maxVel(i))) %if it is larger than double the max velocity, it is considered too big of a step
                largeStep(i,k) = i; %ID of that segment's axis is stored
            else
                largeStep(i,k) = 0;
            end

            %calculating the amount of sample steps over which it has to
            %converge to the generalized trajectory

            convDist(i,k) = abs(round(segmentEndDiff(i)/scalar)); %rounded to zero as it is such an incremental change

            convergenceP(i,k) = meanPose(i,segmentEndID(k)+convDist(i,k)); %the sample it has to converge at



        end

    end


%% applying the convergence

for l = 1:length(segmentEndID) %length(segmentEndID) %COLOUMNS
if ~isempty(largeStep)
    for k = 1:6 %length(largeStep) %length(largeStep) %ROWS

        j = largeStep(k,l);
        
      if j > 0

        velSegmentEnd = segmentEndID(l)+1; %from the end of the velocity segment till the convergence point
        convergenceGoal = segmentEndID(l)+convDist(j,l);
         
        for i = velSegmentEnd:convergenceGoal
            
             convSegment(j,l) = convergenceP(j,l) - segmentEnd(j,l); %change it has to make 

             sign = convSegment(j,l)/abs(convSegment(j,l)); %decision if it is upwards or downwards curving
                    
            %convergence based on the direction 
           if sign == 1

                x = -log((segmentEnd(j,l)-(convergenceP(j,l)+1))/-1);
                Evaldas(l) = ((convergenceGoal-i)/convDist(j,l))*x;
                newTrajectory(j,i) = -exp(-Evaldas(l))+(convergenceP(j,l)+1); %exponential function for convergence (downwards curving)
                
                %prevent overshooting
                
                if newTrajectory(j,i) >= meanPose(j,i)
                   newTrajectory(j,i) =  meanPose(j,i);
                end
          
           else %if sign is negative
               
                x = -log((segmentEnd(j,l)-(convergenceP(j,l)-1))/1);
                Evaldas(l) = ((convergenceGoal-i)/convDist(j,l))*x;
                newTrajectory(j,i) = exp(-Evaldas(l))+(convergenceP(j,l)-1); %(upwards curving)
                
                %undercrossing
                
                if newTrajectory(j,i) <= meanPose(j,i)
                   newTrajectory(j,i) =  meanPose(j,i);
                end
                
           end
           
        end

      end
    end
end
end
end

%% scaling down the values

for j = 1:6
    
    newTrajectory(j,:) = newTrajectory(j,:)/unitScale;
     
end

%% 2plot used for debugging purposes

for k = 1:length(meanPose)
        % plotting the converged solution
    plotNum = 1;
    for i = 1:axisNumber
                        
            subplot(axisNumber, 1, plotNum);
            plot(newTrajectory(i,:),'b')
            hold on; 

            %cycle between the 6 subplots
            if plotNum == axisNumber
                plotNum = 0;
            end 
            plotNum= plotNum+1;
            
    end
     
end

%% 3d plot used for debugging

    figure('Name','3D trajectory plot','NumberTitle','off');

    for k = 1:length(demoSampled)

        plotVariable = cell2mat(demoSampled(1,k));

        plot3(plotVariable(1,:),plotVariable(2,:),plotVariable(3,:));
        hold on;

    end

    %mean Trajectory

    plot3(meanPose(1,:), meanPose(2,:), meanPose(3,:),'r.-')


    %new trajectory

    counter =1;

    for i = 2:length(newTrajectory)

        if counter == 10

        origin = [newTrajectory(1,i), newTrajectory(2,i), newTrajectory(3,i)];

        unitX = [1;0;0]*5;
        unitY = [0;1;0]*5;
        unitZ = [0;0;1]*2;

        rotationX = newTrajectory(4,i);
        rotationY = newTrajectory(5,i);
        rotationZ = newTrajectory(6,i);

        rotM = eul2rotm([rotationZ rotationY rotationX]);

        lineX = rotM*unitX;
        lineY = rotM*unitY;
        lineZ = rotM*unitZ;

        plot3([origin(1) origin(1)+lineX(1)], [origin(2) origin(2)+lineX(2)], [origin(3) origin(3)+lineX(3)],'r', 'LineWidth', 3);
        plot3([origin(1) origin(1)+lineY(1)], [origin(2) origin(2)+lineY(2)], [origin(3) origin(3)+lineY(3)],'g','LineWidth', 3);
        plot3([origin(1) origin(1)+lineZ(1)], [origin(2) origin(2)+lineZ(2)], [origin(3) origin(3)+lineZ(3)],'b','LineWidth', 3);
        hold on;

        counter = 1;

        else

            plot3(newTrajectory(1,i), newTrajectory(2,i), newTrajectory(3,i),'b.-')

            counter = counter+1;

        end

    end



end