function absDiff = absoluteDifference(targetCoordinate, currentCoordinate)

    xdiff = abs(targetCoordinate(1,:)-currentCoordinate(1));
    ydiff = abs(targetCoordinate(2,:)-currentCoordinate(2));
    zdiff = abs(targetCoordinate(3,:)-currentCoordinate(3));
    Rdiff = abs(targetCoordinate(4,:)-currentCoordinate(4));
    Pdiff = abs(targetCoordinate(5,:)-currentCoordinate(5));
    Ydiff = abs(targetCoordinate(6,:)-currentCoordinate(6));
    
    absDiff = [xdiff; ydiff; zdiff; Rdiff; Pdiff; Ydiff];

end