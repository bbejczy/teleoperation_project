function visualFunc(TfMatrix, scalar)


unitX = [1;0;0]*scalar;
unitY = [0;1;0]*scalar;
unitZ = [0;0;1]*scalar;

rotM = [TfMatrix(1:3,1:3)];

origin = [TfMatrix(1,4);TfMatrix(2,4);TfMatrix(3,4)];

lineX = (rotM*unitX)+origin;
lineY = (rotM*unitY)+origin;
lineZ = (rotM*unitZ)+origin;

plotMatrix= [origin lineX lineY lineZ];

plot3([plotMatrix(1,1) plotMatrix(1,2)], [plotMatrix(2,1) plotMatrix(2,2)], [plotMatrix(3,1) plotMatrix(3,2)],'r', 'LineWidth', 3)
hold on;
plot3([plotMatrix(1,1) plotMatrix(1,3)], [plotMatrix(2,1) plotMatrix(2,3)], [plotMatrix(3,1) plotMatrix(3,3)],'g','LineWidth', 3)
plot3([plotMatrix(1,1) plotMatrix(1,4)], [plotMatrix(2,1) plotMatrix(2,4)], [plotMatrix(3,1) plotMatrix(3,4)],'b','LineWidth', 3)

axis([-50 150 -50 150 -50 150])
grid on
end