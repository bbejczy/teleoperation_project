close all;clear all; clc;

%ros initialization
rosshutdown
rosinit('192.168.0.43')
rostopic list
joy = rossubscriber('/sub');
pause(1);
%Accept a connection from any machine on port 30000.

t = tcpip('0.0.0.0', 30000, 'NetworkRole', 'server');

%Open a connection. This will not return until a connection is received.

fopen(t);


%declarations
startOffsetX = 0;
startOffsetY = 0;
startOffsetZ = 0;
startPoseMatrix= [1 0 0 startOffsetX; 0 1 0 startOffsetY; 0 0 1 startOffsetZ; 0 0 0 1];
startTraj= [startOffsetX;startOffsetY;startOffsetZ;0;0;0;0];
newStartingPose = zeros(8,1); %THIS HAS TO BE FED FROM CONTROLLER
loggedDemo = zeros(8,1);
demoDataset = {};

% goal and viapoints definition
goalTrajectory= [50;20;5;0;0;pi/2]; %orientation in radiants
viaPoint1 = [startOffsetX;startOffsetY;startOffsetZ+30;0;0;0]; %"vial pull" viapont situated directly above start
viaPoint2 = [45;20;30;0;0;pi/2];

%scalars for plot size
scalarcurrent = 10;
scalargoal = 20;
scalarvia = 10;
view_angle=[160;0];

%tolerances for plotting
tolVia= 5;
tolAngle = 10;
tolGoal = 5;

%sequences and decisions
seqCount = 1;
terminateSubscriber = 1;
terminatePublisher = 1;
mainTerminate = 0;
demoEvaluation = 1;
savedDemoEntry = 0;


figure('Name','Demonstration Visualization','NumberTitle','off');
hold on;

while(~mainTerminate)
    
if (t.BytesAvailable)
    testData = fread(t, t.BytesAvailable)
end

if (testData(1) == 4)
    fprintf('Starting recording\n')
    terminateSubscriber = 0;
end

if (testData(1) == 6)
    fprintf('Starting publishing the new trajectory\n')
    terminatePublisher = 0;
    pass = 0;
end
    
    if terminatePublisher == 0
    controller = receive(joy, 10); %second parameter is the timeout
    newStartingPose(1) = controller.Axes(1);
    newStartingPose(2) = controller.Axes(2);
    newStartingPose(3) = controller.Axes(3); 
    newStartingPose(7) = 1;
    end
    
    %reset values
    startPoseMatrix= [1 0 0 startOffsetX; 0 1 0 startOffsetY; 0 0 1 startOffsetZ; 0 0 0 1];
    startTraj= [startOffsetX;startOffsetY;startOffsetZ;0;0;0;0];
    startTrajPlot = startTraj;
    viaPoint1 = [startOffsetX;startOffsetY;startOffsetZ+30;0;0;0];
    loggedDemo = zeros(8,1);
    seqCount = 1;
    view_angle=[160;0];
    

    while (~terminateSubscriber)

        controller = receive(joy, 10); %second parameter is the timeout

        %receiving the controller input from spacenav
        spacenav = [controller.Axes(1), controller.Axes(2), controller.Axes(3), controller.Axes(4), controller.Axes(5), controller.Axes(6)];
        buttons = [controller.Buttons(1), controller.Buttons(2)];

        %positions
        xAxis = spacenav(1);
        yAxis = spacenav(2);
        zAxis = spacenav(3);
        %orientations
        xRoll = spacenav(4)*pi/180;
        yPitch = spacenav(5)*pi/180;
        zYaw = spacenav(6)*pi/180;
        %buttons
        buttonLeft = double(buttons(1));
        buttonRight = double(buttons(2));


        %displaying the change of trajectory
        currentPoseMatrix = transl(xAxis,yAxis,zAxis)*rpy2tr(xRoll,yPitch,zYaw);

        

        %startPoseMatrix = currentPoseMatrix; %feed back as a new starting point
        
        rotationMatrix = rotm2eul(currentPoseMatrix(1:3,1:3));
        
        %logging the changed trajectories into 'saved' 
        changedTraj = [currentPoseMatrix(1,4); currentPoseMatrix(2,4); currentPoseMatrix(3,4); rotationMatrix(3);rotationMatrix(2);rotationMatrix(1);seqCount;0]
        loggedDemo(:,seqCount) = changedTraj;
        startTraj = loggedDemo(:,seqCount);


        visualFunc(currentPoseMatrix, scalarcurrent);
        
        %plotting the static starting position
        visualFuncStatic(startTrajPlot, scalargoal);
        

        

        
        %%%%%%%%%%% FOR MATLAB DEBUGGING PURPOSES %%%%%%%%%%%%

        %viewing angle rotation
       % view_angle = view_angle+[1-buttonLeft;-(1-buttonRight)];
        aa = double(90+view_angle(1,1)+view_angle(2,1));
        view(aa,40)

        %absolute differences for plotting
        absDiffVia1 = absoluteDifference(viaPoint1, loggedDemo(:,seqCount));
        absDiffVia2 = absoluteDifference(viaPoint2, loggedDemo(:,seqCount));   
        absDiffGoal = absoluteDifference(goalTrajectory, loggedDemo(:,seqCount));

        %logical statement of visualizing the viapoints based on the differences
        logicVia1 = absDiffVia1(1) <= tolVia && absDiffVia1(2) <= tolVia && absDiffVia1(3) <= tolVia && absDiffVia1(4) <= tolAngle && absDiffVia1(5) <= tolAngle && absDiffVia1(6) <= tolAngle;
        logicVia2 = absDiffVia2(1) <= tolVia && absDiffVia2(2) <= tolVia && absDiffVia2(3) <= tolVia && absDiffVia2(4) <= tolAngle && absDiffVia2(5) <= tolAngle && absDiffVia2(6) <= tolAngle;

        %goaltrajectory plot
        visualFuncStatic(goalTrajectory,scalargoal);

        % Viapoint 1 indicator disappears once it is reached with the tolerance
        if not(logicVia1)       
            visualFuncStatic(viaPoint1, scalarvia);       
        end

        % Viapoint 2 indicator disappears once it is reached with the tolerance
        if not(logicVia2)
             visualFuncStatic(viaPoint2, scalarvia);
        end

        %exit loop
    %     if absDiffGoal(1) <= tolGoal && absDiffGoal(2) <= tolGoal && absDiffGoal(3) <= tolGoal && absDiffGoal(4) <= tolAngle && absDiffGoal(5) <= tolAngle && absDiffGoal(6) <= tolAngle
    %         disp('Target reached')        
    %         if rem(seqCount, 2) == 0
    %             return;
    %         end
    %     end

        hold off;
        
        %%%%%%%%%%% FOR MATLAB DEBUGGING PURPOSES [END] %%%%%%%%%%%%

        seqCount = seqCount+1; %sequence number fed into the matrix
        if (t.BytesAvailable)
            testData = fread(t, t.BytesAvailable)
        end
        if (testData(1) == 5)
        terminateSubscriber = 1;
        end

        demoEvaluation = 0;
    end
    
    %% Saving or discarding the demonstration

    while(~demoEvaluation)
        if (t.BytesAvailable)
        testData = fread(t, t.BytesAvailable)
        end
%         promptEvaluation = 'Do you want to save? [y/n] + enter: ';
%         demoDecision = input(promptEvaluation,'s');
%         if not(isempty(demoDecision))
            
            if  testData(1) == 3
                fprintf('trajectory saved\n')
                demoEvaluation = 1;
                savedDemoEntry = savedDemoEntry+1;
                demoDataset{savedDemoEntry} = loggedDemo;
                save('dataSetObject','demoDataset')
                
                
            end
       
            if testData(1) == 2   
                fprintf('trajectory discarded\n')
                demoEvaluation = 1;
            end
         
%         end
        
        pause(2)
        
    end
    
    %% publishing the new trajectory

    while (~terminatePublisher)
        
        if pass == 0
        newTrajectory = dataProcessing(newStartingPose);
        end
        pass = 1;
        
        for i = 1:length(newTrajectory)
            
            sendTrajectory = newTrajectory(:,i)
            
            pub = rospublisher('/newtraj', 'sensor_msgs/Joy');
            
            msg = rosmessage(pub);
            msg.Axes(1) = sendTrajectory(1);
            msg.Axes(2) = sendTrajectory(2);
            msg.Axes(3) = sendTrajectory(3);
            rotmat = eul2rotm([sendTrajectory(4) sendTrajectory(5) sendTrajectory(6)]);
            msg.Axes(4) = rotmat(1,1);
            msg.Axes(5) = rotmat(1,2);
            msg.Axes(6) = rotmat(1,3);
            msg.Axes(7) = rotmat(2,1);
            msg.Axes(8) = rotmat(2,2);
            msg.Axes(9) = rotmat(2,3);
            msg.Axes(10) = rotmat(3,1);
            msg.Axes(11) = rotmat(3,2);
            msg.Axes(12) = rotmat(3,3);
            msg.Buttons(1) = 1;

            send(pub,msg);
            
            pause(0.01);
            
        end
        msg.Buttons(1) = 0;
        send(pub,msg);
        
        fprintf('new Trajectory has been sent\n\n\n');
        pause(2)
        
        promptSendAgain = 'Do you want to send it again? [y/n] + enter: ';
        sendAgain = input(promptSendAgain,'s');
        if not(isempty(sendAgain))
            if  sendAgain == 'y'
                fprintf('Sending the trajectory again\n')
            elseif sendAgain == 'n'
                fprintf('Terminating publisher\n')
                terminatePublisher = 1;
            end
                
        end
        
    end
    

end


