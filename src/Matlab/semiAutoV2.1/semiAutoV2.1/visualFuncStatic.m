function visualFuncStatic(coordinateFrame, scalar)

TfMatrix = transl(coordinateFrame(1,1),coordinateFrame(2,1),coordinateFrame(3,1))*rpy2tr(coordinateFrame(4,1),coordinateFrame(5,1),coordinateFrame(6,1));

unitX = [1;0;0]*scalar;
unitY = [0;1;0]*scalar;
unitZ = [0;0;1]*scalar;

rotM = [TfMatrix(1:3,1:3)];

origin = [TfMatrix(1,4);TfMatrix(2,4);TfMatrix(3,4)];

lineX = (rotM*unitX)+origin;
lineY = (rotM*unitY)+origin;
lineZ = (rotM*unitZ)+origin;

plotMatrix= [origin lineX lineY lineZ];

plot3([plotMatrix(1,1) plotMatrix(1,2)], [plotMatrix(2,1) plotMatrix(2,2)], [plotMatrix(3,1) plotMatrix(3,2)],'m', 'LineWidth', 3)
hold on;
plot3([plotMatrix(1,1) plotMatrix(1,3)], [plotMatrix(2,1) plotMatrix(2,3)], [plotMatrix(3,1) plotMatrix(3,3)],'m','LineWidth', 3)
plot3([plotMatrix(1,1) plotMatrix(1,4)], [plotMatrix(2,1) plotMatrix(2,4)], [plotMatrix(3,1) plotMatrix(3,4)],'m','LineWidth', 3)

axis([-25 75 -25 75 -25 75])
grid on

end