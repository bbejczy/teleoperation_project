function TH_avoided = avoidance(TH_current,p,k,max_iterations,safe_distance,counter)

%% initiation
 % current joint positions in cartesian space
[R07_current,J_current] = fowKin(TH_current);
%distance between the obstacle and the link of interest
d1_current = d1(k,J_current,p); 
%distance between the eef and the tip on the link of the interest
d2_current = d2(k,J_current,J_current);

%% numeric differentiation
%differentiation step size
TH_change = 0.025;

for j = 1:7
   TH_diffj = TH_current;
   TH_diffj(j,1) = TH_current(j,1)+TH_change;
    
   [R07_diffj,J_diffj] = fowKin(TH_diffj);
   
   D1_diff(j,1) = d1(k,J_diffj,p)-d1_current;
   D2_diff(j,1) = d2(k,J_diffj,J_current)-d2_current;
end

%% guess 
%descending effect on the d1
[D1_sorted,sortID1] =  sort(abs(D1_diff),'descend');
%ascending effect on d2
[D2_sorted,sortID2] = sort(abs(D2_diff),'ascend');

if (k == 2 || k == 4) 
    %th1 is always selected to address the distance maximization
    aID = 1;
    %initial guess scaling factor 
    Ka = 0.1;
    %th2 is arbitrarily selected since bID is not used 
    bID = 2;
    
    %initial guess for the solution
    TH_guess = TH_current;
    TH_guess(aID,1) = TH_guess(aID,1)+D1_diff(aID,1)*Ka;
    

elseif ( k == 6)

    % %% adhering to both criteria of d1 maximization and d2 minimization by
    % tangent approach
     alpha = atan(D2_diff./D1_diff);
     alpha = alpha + [2;0;0;0;0;0;0]; % disabling th1 from solving for k6 avoidance (was unstable)
    [alpha_sorted,sortID_alpha] = sort(abs(alpha),'ascend');
    
    aID = sortID_alpha(1,1) 
    bID = sortID_alpha(2,1)


    Ka = 0.001;
    Kb = 0.001;

    TH_guess = TH_current;
    if(alpha(aID,1)<0)
        TH_guess(aID,1) = TH_guess(aID,1)+(-pi-alpha(aID,1))*Ka;
    else
        TH_guess(aID,1) = TH_guess(aID,1)+(pi+alpha(aID,1))*Ka;
    end

    if(alpha(bID,1)<0)
        TH_guess(bID,1) = TH_guess(bID,1)+(-pi-alpha(bID,1))*Kb;
    else
        TH_guess(bID,1) = TH_guess(bID,1)+(pi+alpha(bID,1))*Kb;
    end

end


%% iterative solution finding
%solution goal condition
F_goal = [safe_distance; d2_current];
%initial set of joints that will be modified 
qi = [TH_guess(aID,1);TH_guess(bID,1)];
THi = TH_guess;
qi1 = qi;

for i = 1:max_iterations
    [R07_i,Ji] = fowKin(THi);
    %d1 and d2 constraints are found with every iteration 
    d1i = d1(k,Ji,p);
    d2i = d2(k,Ji,J_current);
    %current state 
    Fi = [d1i;d2i];
    %difference to the goal state
    Fdi = F_goal-Fi;
    %Jacobian function 
    Jaci2 = Jacobian2(Ji,aID,bID,THi,d1i,d2i,k,p,qi); % can be restructured

    if(det(Jaci2)==0) %Jacobian is singular
        qi1 = qi1;
    else
        qi1 = qi + Jaci2*1\Fdi ; 
    end

    
    THi(aID,1) = qi1(1,1);
    THi(bID,1) = qi1(2,1);
    
    qi = qi1;

end

TH_avoided = THi;

