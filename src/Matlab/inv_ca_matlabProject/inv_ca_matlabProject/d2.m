function d2 = d2(k,J,Jin)

if (k == 6 )
    d2 = sqrt(dot((Jin(:,7)-J(:,k+1)),(Jin(:,7)-J(:,k+1))));
elseif (k == 4 )
     d2 = 0;
elseif (k == 2)
    d2 = 0;
end
