%% Finding the virtual joints

clear all
close all

rosshutdown;
rosinit('192.168.0.43');
ptree = rosparam;
set(ptree,'/use_sim_time',false)
joy = rossubscriber('/sub');
state = rospublisher('/iiwa/joint_states','sensor_msgs/JointState'); % publishing  curren EEF position 
msg = rosmessage('sensor_msgs/JointState');
h = rosmessage('std_msgs/Header');

array. joints = java_array('java.lang.String',7);
joints(1) = java.lang.String('iiwa_joint_1');
joints(2) = java.lang.String('iiwa_joint_2');
joints(3) = java.lang.String('iiwa_joint_3');
joints(4) = java.lang.String('iiwa_joint_4');
joints(5) = java.lang.String('iiwa_joint_5');
joints(6) = java.lang.String('iiwa_joint_6');
joints(7) = java.lang.String('iiwa_joint_7');
joints_cell = cell(joints);
msg.Name = joints_cell;
uint32 s;
s = 0;
%link lengths
dbs = 34/100; 
dse = 40/100;
dew = 40/100;
dwf = 12.55/100;
%DH parameters
DH = [0 0       dbs ;
      0 -pi/2   0   ;
      0 pi/2    dse ;
      0 pi/2    0   ;
      0 -pi/2   dew ;
      0 -pi/2   0   ;
      0 pi/2    dwf ];
%joint limits (rad)
THU = [170*pi/180;120*pi/180;170*pi/180;     0.001*pi/180; 170*pi/180;  120*pi/180;   175*pi/180];
THL = [-170*pi/180;0.001*pi/180;-170*pi/180; -120*pi/180;-170*pi/180; 0.001*pi/180;-175*pi/180];
%target pose:
R07=  eul2rotm([0 pi 0]);
p07 = [0.0 0.4 0.3]';
%arm configuration parameters
GC2 = 1;
GC4 = -1; 
GC6 = 1;
%initial geometric arm description
p02 = [0 0 dbs]';
p24 = [0 dse 0]';
p46 = [0 0 dew]';
p67 = [0 0 dwf]';
p04 = p02+p24; 
p06 = p04+p46; 
p02n = p02;
p04n = p04;
p06n = p06;

p26 = p07 - p02 - (R07*p67);
%initial arma angle
Psi = 0;

inputPos = [0.00;0;0];
terminate = 0;
view_angle = [0;0]; % for plotting 
sgPsi = 0;
counter = 0;
%initiation of some matrices
int = zeros(7,4);
direction = zeros(7,4);
ordDirection = zeros(6,6);
TH_currentNorm = zeros(7,1);
% initial interval ID
m=1; 
n = 0; % for debuging
time = 0; % for testing
 
%% loop
%loop condition for subscribing to /joy
 while(~terminate)
%loop condition for subscribing to interpreter.cpp
% while(true)

 controller = receive(joy, 10);
 %inputs tuned for subscribing to /joy node
 Psi = Psi-controller.Axes(7)*pi/180 * 2;% 2degrees
 Psi = Psi+controller.Axes(8)*pi/180 * 0.1;% 2degrees
 inputPos  = [controller.Axes(1); controller.Axes(2); controller.Axes(3)];
 %view_angle = view_angle+[1-controller.Axes(3);-(1-controller.Axes(6))];
 %terminate = 0; %terminate = controller.Buttons(1);
 p07 = inputPos; %p07 = p07 + inputPos;
%inputs for subscribing to interpreter.cpp
%  p07 = [controller.Axes(1);controller.Axes(2);controller.Axes(3)]; %absolute position [x y z]
%  R07 = eul2rotm([controller.Axes(6) controller.Axes(5) controller.Axes(4)]); %orientation [z y x]
 
 p26 = p07 - p02 - (R07*p67);

%% virtual arm joint values

if (((norm(p26)^2 - dse^2 - dew^2)/(2*dse*dew))<1) %prevents getting out of reach
    th4v = GC4*acos((norm(p26)^2 - dse^2 - dew^2)/(2*dse*dew));
end


th1v = atan2(p26(2,1),p26(1,1));

if ((dse^2 + norm(p26)^2 - dew^2)/(2*dse*norm(p26))<1) %prevents getting out of reach
    phi = acos((dse^2 + norm(p26)^2 - dew^2)/(2*dse*norm(p26)));
end

th2v = atan2(sqrt(p26(1,1)^2+p26(2,1)^2),p26(3,1))+GC4*phi;

th3v = 0;

thv = [th1v th2v th3v th4v 0 0 0]';

l0 = [0 0 0]'; % base position

T0 = eye(4); % base orientation 

% the loop for R07=R01*R12*...*R67 of the virtual arm;
for i = 1:7
    aim1 = DH(i,1);
    alim1 = DH(i,2);
    di = DH(i,3);
    thi = thv(i,1);
    %generic R matrix based on the DH parameters
    Ri = [cos(thi) -sin(thi) 0;
               sin(thi)*cos(alim1) cos(thi)*cos(alim1) -sin(alim1);
               sin(thi)*sin(alim1) cos(thi)*sin(alim1) cos(alim1)];

    Pi = [aim1; 
               -di*sin(alim1);
               di*cos(alim1)];

    Ti = [Ri Pi; 0 0 0 1];

    T0i = T0 * Ti;

    l1 = [T0i(1,4) T0i(2,4) T0i(3,4)]';
    
    if(i == 2)
        p02v = l1;
    end
    if(i == 3)
       R03v = T0i(1:3,1:3); 
    end
    if(i == 4)
        p04v = l1;
        R34v = Ri;
    end
    if(i == 6)
        p06v = l1;
    end

    l0 = l1;
    T0 = T0i;
end

%% Finding the real arm joint angles

%normalisation
p26norm = p26/norm(p26);
%cross product matrix for p26
p26cross = [0 -p26norm(3,1) p26norm(2,1);
            p26norm(3,1) 0 -p26norm(1,1);
           -p26norm(2,1) p26norm(1,1) 0];
%Rodrigues formula for defining the roation of Psi around p26
R0psi = eye(3)+sin(Psi+pi)*p26cross+(1-cos(Psi+pi))*p26cross^2;

R03 = R0psi * R03v;
 %one of the ways of assembling the parameters for trig functions (useful in joint limit analysis)
 As = p26cross*R03v;
 Bs = -p26cross^2*R03v;
 Cs = (p26norm * p26norm')*R03v;
 
th1r = atan2(GC2*(As(2,3)*sin(Psi)+Bs(2,3)*cos(Psi)+Cs(2,3)),...
    GC2*(As(1,3)*sin(Psi)+Bs(1,3)*cos(Psi)+Cs(1,3))); 

th2r = GC2*acos(As(3,3)*sin(Psi)+Bs(3,3)*cos(Psi)+Cs(3,3));

th3r = atan2(GC2*(As(3,2)*sin(Psi)+Bs(3,2)*cos(Psi)+Cs(3,2)),...
    GC2*(-As(3,1)*sin(Psi)-Bs(3,1)*cos(Psi)-Cs(3,1)));

R34 = R34v;

th4r = th4v;

Aw = R34'*As'*R07;
Bw = R34'*Bs'*R07;
Cw = R34'*Cs'*R07;
%parameters used in joint limit analysis
an = [As(2,3);As(3,3);As(3,2);0;Aw(3,3);Aw(2,3);-Aw(2,2)];
ad = [As(1,3);0;-As(3,1);0;-Aw(1,3);0;Aw(2,1)]; 
bn = [Bs(2,3);Bs(3,3);Bs(3,2);0;Bw(3,3);Bw(2,3);-Bw(2,2)];
bd = [Bs(1,3);0;-Bs(3,1);0;-Bw(1,3);0;Bw(2,1)];
cn = [Cs(2,3);Cs(3,3);Cs(3,2);0;Cw(3,3);Cw(2,3);-Cw(2,2)];
cd = [Cs(1,3);0;-Cs(3,1);0;-Cw(1,3);0;Cw(2,1)]; 

  
th5r = pi+atan2(GC6*(Aw(3,3)*sin(Psi)+Bw(3,3)*cos(Psi)+Cw(3,3)),...
    GC6*(-Aw(1,3)*sin(Psi)-Bw(1,3)*cos(Psi)-Cw(1,3)));

th6r = GC6*acos(Aw(2,3)*sin(Psi)+Bw(2,3)*cos(Psi)+Cw(2,3));

th7r = pi+atan2(GC6*(-Aw(2,2)*sin(Psi)-Bw(2,2)*cos(Psi)-Cw(2,2)),...
    GC6*(Aw(2,1)*sin(Psi)+Bw(2,1)*cos(Psi)+Cw(2,1)));
%set of the real arm joint values
thr = [th1r th2r th3r th4r th5r th6r th7r]';

TH_current=thr;
[R07_current,J_current] = fowKin(TH_current);


%% collision avoidance

 %obstacle position
 p = [0.05; 0.25; 0.5];
 %distance thresholds [link2; link4; link6]
 safe_distance = [1.1;1.1;1.2];

 %identifying the link that's closest to the obstacle
 if     (round(d1(2,J_current,p),3) < safe_distance(1,1))
    close_linkID = 2;
    tex = '2';
    
 elseif (round(d1(4,J_current,p),3) < safe_distance(2,1))
    close_linkID = 4;
    tex = '4';
   
 elseif (round(d1(6,J_current,p),3) < safe_distance(3,1))
    close_linkID = 6;
    tex = '6';
   
 else 
     close_linkID = 0;
     tex = 'no';
     
 end
 
 %calling the avoidance function 
if (close_linkID ~= 0)

    [R07_current,J_current] = fowKin(TH_current);
   
    %maximum iterations for newton-rapson loop
    max_iterations = 10;
    %a set of modified angles after avoidance
    TH_avoided = avoidance(TH_current,p,close_linkID,max_iterations,safe_distance(close_linkID/2,1));


%% after avoidance
    %arm angle is recalculated based on the modified joint set (th1)
    if (close_linkID == 2 || close_linkID == 4)

        [R07_avoided,J_avoided]=fowKin(TH_avoided);
         
        p02n = J_avoided(:,2);
        p24n = J_avoided(:,4)-J_avoided(:,2);
  
        p46n = J_current(:,6)-J_avoided(:,4);
        p67n = p67;

        p04n = p02n+p24n; 
        p06n = p04n+p46n; 
        p07n = p06n+p67n;
        %new Psi(arm angle) is found to correspond to the new th1 and
        %maintain the original eef pose
        vsewv = cross(((p04v-p02v)/norm(p04v-p02v)),((p06v-p02v)/norm(p06v-p02v)));
        vsewr = cross(((p04n-p02n)/norm(p04n-p02n)),((p06n-p02n)/norm(p06n-p02n)));
        sgPsi = dot(cross(vsewv/norm(vsewv),vsewr/norm(vsewr)),p26);
        sgPsi = sgPsi / sqrt(sgPsi^2);
        Psi = sgPsi*acos(dot(vsewv/norm(vsewv),vsewr/norm(vsewr)));

    elseif(close_linkID == 6)
        %the modified set is taken directly if avoidance is applied for
        %link6
        TH_current = TH_avoided;
        
    end
end
   %manipulator and the obstacle are plotted
     figure(1)
     set(gcf, 'WindowState', 'maximized');
     FowKinPlotter(TH_current,0,view_angle)
     plot3(p(1,1),p(2,1),p(3,1),'go','MarkerSize',12)
     text(0,0,0.5,tex)

%% output (TH_current)

   violation = 0;
   %normalizing joint values to [-pi;pi] from [0;2*pi]
   TH_currentNorm = TH_current;
   for i = 1:7
        if (TH_current(i,1) > pi)
            TH_currentNorm(i,1) = TH_current(i,1) - 2*pi;
        end
        %checks if any of the joints are crossing the limits 
        if(TH_currentNorm(i,1) < THL(i,1) || TH_currentNorm(i,1) > THU(i,1))
            violation = 1;
        end
        
   end 
    %old set of joint values are kept if there is a violation
%    if(violation)
%         TH_current = TH_currentPrev;
%    else
%        TH_currentPrev = TH_current;
%    end
    
    [R07,J_07]=fowKin(TH_current);
    p07 = J_07(:,7);
    TH_current;
    msg.Position(1) = TH_current(1);
    msg.Position(2) = TH_current(2);
    msg.Position(3) = TH_current(3);
    msg.Position(4) = TH_current(4);
    msg.Position(5) = TH_current(5);
    msg.Position(6) = TH_current(6);
    msg.Position(7) = TH_current(7);
    s = s + 1;
    h.Seq = s;
    h.Stamp = rostime("now");
    h. FrameId = '';
    mag.Header = h;
send(state,msg); %publish

    %% joint limits
    
    %int matrix is cleared 
    int = zeros(7,4);
     out = [1;1;1;1;1;1;1];
  %pivot joints (1,3,5,7)
    for piv = 1:4
        
        ii = 1+(piv-1)*2;
        if(ii < 4)
            GCk = GC2;
            %variable for fixing some geometrical intricacies for th5 and
            %th7
            fix5 = 1;
        else
            GCk = GC6;
            fix5 = -1;
        end
        
        % joint limits
        thiU = THU(ii,1);
        thiL = THL(ii,1);
        %intermediate values
        at = GCk*(cn(ii,1)*bd(ii,1)-bn(ii,1)*cd(ii,1));
        bt = GCk*(an(ii,1)*cd(ii,1)-cn(ii,1)*ad(ii,1));
        ct = GCk*(an(ii,1)*bd(ii,1)-bn(ii,1)*ad(ii,1));

        ap = GCk*((cd(ii,1)-bd(ii,1))*tan(thiU)+(bn(ii,1)+cn(ii,1)));
        bp = 2*GCk*(ad(ii,1)*tan(thiU)-an(ii,1));
        cp = GCk*((bd(ii,1)+cd(ii,1))*tan(thiU)-(bn(ii,1)+cn(ii,1)));
        %Psi(thiU) - arm angle at upper limit crossings
        PSI1u = 2*atan((-bp+sqrt(bp^2-4*ap*cp))/(2*ap));
        PSI2u = 2*atan((-bp-sqrt(bp^2-4*ap*cp))/(2*ap));

        ap = GCk*((cd(ii,1)-bd(ii,1))*tan(thiL)+(bn(ii,1)+cn(ii,1)));
        bp = 2*GCk*(ad(ii,1)*tan(thiL)-an(ii,1));
        cp = GCk*((bd(ii,1)+cd(ii,1))*tan(thiL)-(bn(ii,1)+cn(ii,1)));
        %Psi(thiL) - arm angle at lower limit crossings
        PSI1l = 2*atan((-bp+sqrt(bp^2-4*ap*cp))/(2*ap));
        PSI2l = 2*atan((-bp-sqrt(bp^2-4*ap*cp))/(2*ap));
        %storing into matrix form
        PSIorg =  [PSI1u; PSI2u; PSI1l; PSI2l];
        PSI =  [PSI1u*isreal(PSI1u) ; PSI2u*isreal(PSI2u); PSI1l*isreal(PSI1l); PSI2l*isreal(PSI2l)];
        
        
        for kk = 1:4
            u = GCk*(an(ii,1)*sin(PSI(kk,1))+bn(ii,1)*cos(PSI(kk,1))+cn(ii,1));
            v = GCk*(ad(ii,1)*sin(PSI(kk,1))+bd(ii,1)*cos(PSI(kk,1))+cd(ii,1));
            dTHdPsi = fix5*(at*sin(PSI(kk,1))+bt*cos(PSI(kk,1))+ct)/(u^2+v^2);

            thPsi = atan2(GCk*(an(ii,1)*sin(PSI(kk,1))+bn(ii,1)*cos(PSI(kk,1))+cn(ii,1)),...
            GCk*(ad(ii,1)*sin(PSI(kk,1))+bd(ii,1)*cos(PSI(kk,1))+cd(ii,1)));
            %the direction at the cross points defines wether the Psi
            %segment is leaving the feasible region [thiL,thiU] or entering it 
            if(isreal(dTHdPsi) && isreal(thPsi))
                direction(ii,kk) =  (-1)*sign(dTHdPsi)*sign(thPsi)*isreal(PSIorg(kk,1));
                %corresponding Psi(thiU) or Psi(thiL)
                int(ii,kk) = PSI(kk,1);
            end
        end

        
        if(abs(at^2+bt^2-ct^2) < 0.00001) % singularity condition (address)
            stat = 1 ;
            PsiSing = 2*atan(at/(bt-ct));
            
        elseif (at^2+bt^2-ct^2 > 0) % two stationary points
            stat = 0;
             % stationary points (function minimum and maximum)
            Psi01 = 2*atan((at+sqrt(at^2+bt^2-ct^2))/(bt - ct));
            Psi02 = 2*atan((at-sqrt(at^2+bt^2-ct^2))/(bt - ct));       
            %theta values at stationary points
            thPsi01 = atan2(GCk*(an(ii,1)*sin(Psi01)+bn(ii,1)*cos(Psi01)+cn(ii,1)),...
            GCk*(ad(ii,1)*sin(Psi01)+bd(ii,1)*cos(Psi01)+cd(ii,1)));
            thPsi01 = sign(thPsi01)*abs(abs(thPsi01)+(pi*fix5-pi)/2);
        
            thPsi02 = atan2(GCk*(an(ii,1)*sin(Psi02)+bn(ii,1)*cos(Psi02)+cn(ii,1)),...
            GCk*(ad(ii,1)*sin(Psi02)+bd(ii,1)*cos(Psi02)+cd(ii,1)));
            thPsi02 = sign(thPsi02)*abs(abs(thPsi02)+(pi*fix5-pi)/2);
            %theta value at Psi = 0
            thPsi0 = atan2(GCk*(an(ii,1)*sin(0)+bn(ii,1)*cos(0)+cn(ii,1)),...
            GCk*(ad(ii,1)*sin(0)+bd(ii,1)*cos(0)+cd(ii,1)));
            thPsi0 = sign(thPsi0)*abs(abs(thPsi0)+(pi*fix5-pi)/2);

            if(norm(PSI) == 0)
                out(ii,1) = 0;
            end
            % addressing the full span case
            if((thPsi01 < thiU) && (thPsi02 > thiL) && thPsi0 > thiL && thPsi0 < thiU)
                direction(ii,:) = [0 0 0 0]; 
                int(ii,:) = [0 0 0 0];
                out(ii,1) = 1;
               
            end
            

                
        else %at^2+bt^2-ct^2 < 0 % without stationary points
            stat = 2;
            %thetas at stationary points (inflection point(s))
            thPsiPi = atan2(GCk*(an(ii,1)*sin(-pi)+bn(ii,1)*cos(-pi)+cn(ii,1)),...
            GCk*(ad(ii,1)*sin(-pi)+bd(ii,1)*cos(-pi)+cd(ii,1)));
            thPsiPi = sign(thPsiPi)*abs(abs(thPsiPi)+(pi*fix5-pi)/2);
            
            thPsiPi2 = atan2(GCk*(an(ii,1)*sin(-pi+0.001)+bn(ii,1)*cos(-pi+0.001)+cn(ii,1)),...
            GCk*(ad(ii,1)*sin(-pi+0.001)+bd(ii,1)*cos(-pi+0.001)+cd(ii,1)));
            thPsiPi2 = sign(thPsiPi2)*abs(abs(thPsiPi2)+(pi*fix5-pi)/2);
            %specific solution for the case
            if(thPsiPi2 > thPsiPi)
                direction(1,:) = [direction(1,1) 0 direction(1,3) 0];
            else
                direction(1,:) = [0 direction(1,2) 0 direction(1,4)];
            end
        end
    end
     
  %  hinge joints (2,6)
    for hin = 1:2
       
        ii = 2 + (hin-1)*4;
        
        if(ii < 4)
            GCk = GC2;
        else
            GCk = GC6;
        end
              
        thiU = THU(ii,1);
        thiL = THL(ii,1);
        
        Psi01 = 2*atan(-bn(ii,1)+sqrt(an(ii,1)^2+bn(ii,1)^2)/an(ii,1));
        Psi02 = 2*atan(-bn(ii,1)-sqrt(an(ii,1)^2+bn(ii,1)^2)/an(ii,1));

        PSI1u = 2*atan((an(ii,1)+sqrt(an(ii,1)^2+bn(ii,1)^2-(cn(ii,1)-cos(thiU))^2))/(cos(thiU)+bn(ii,1)-cn(ii,1)));
        PSI2u = 2*atan((an(ii,1)-sqrt(an(ii,1)^2+bn(ii,1)^2-(cn(ii,1)-cos(thiU))^2))/(cos(thiU)+bn(ii,1)-cn(ii,1)));

        PSI1l = 2*atan((an(ii,1)+sqrt(an(ii,1)^2+bn(ii,1)^2-(cn(ii,1)-cos(thiL))^2))/(cos(thiL)+bn(ii,1)-cn(ii,1)));
        PSI2l = 2*atan((an(ii,1)-sqrt(an(ii,1)^2+bn(ii,1)^2-(cn(ii,1)-cos(thiL))^2))/(cos(thiL)+bn(ii,1)-cn(ii,1)));

        PSI = [PSI1u; PSI2u; PSI1l; PSI2l];        

        THUL = [thiU; thiU; thiL; thiL];

        for kk = 1:4

             dTHdPsi = -GCk*((an(ii,1)*cos(PSI(kk,1))-bn(ii,1)*sin(PSI(kk,1)))/sin(THUL(kk,1)));
             
             thPsi = GCk*acos(an(ii,1)*sin(PSI(kk,1))+bn(ii,1)*cos(PSI(kk,1))+cn(ii,1));
             
            if(isreal(dTHdPsi) && isreal(thPsi))
                direction(ii,kk) =  (-1)*sign(dTHdPsi)*sign(thPsi);
                int(ii,kk) = PSI(kk,1);
            end

        end


                
        if(an(ii,1)^2+bn(ii,1)^2 > 0) % two stationary points
            stat = 2;
            
            thPsi01 = GCk*acos(an(ii,1)*sin(Psi01)+bn(ii,1)*cos(Psi01)+cn(ii,1));
            thPsi02 = GCk*acos(an(ii,1)*sin(Psi02)+bn(ii,1)*cos(Psi02)+cn(ii,1));
            thPsi0 = GCk*acos(an(ii,1)*sin(0)+bn(ii,1)*cos(0)+cn(ii,1));
            %no limitations
            if((thPsi01 < thiU && thPsi01 > thiL) && (thPsi02 < thiU && thPsi02 > thiL))
                direction(ii,:) = [0 0 0 0];
                int(ii,:) = [0 0 0 0];
            end
            %no solutions
            if(~(thPsi01 < thiU && thPsi02 > thiL) && thPsi0 > thiU )
               out(ii,1) = 0; 
            end
            
        elseif (an(ii,1)^2+bn(ii,1)^2==0) %no stationary points
            stat = 1;
        else %should not exist 
            stat = 0;
        end
        
    end

    %used for defining the presence of crossing points at -pi and pi
    out = [out(1:3);out(5:7)];
    oneVec = [1;1;1;1;1;1].*out;
    %all Psi values at limit crossings stored
    Int = [-pi*oneVec ([int(1:3,:);int(5:7,:)]) pi*oneVec];
    %crossing directions stored
    Direction = [oneVec ([direction(1:3,:);direction(5:7,:)]) -oneVec];
    [Int,intID] = sort(Int,2,'ascend'); 
    intPair = zeros(6,6);
    pair = zeros(6,6);
    
    %reordering the direction to match the ascending manner of Psis
    for j = 1:6
        ordDirection(j,:) = Direction(j,intID(j,:)); 
    end

    for j = 1:6
    
        row = ordDirection(j,:);
        ids = [2 5];
        
        if ((leftmost(2,5,row,1)>leftmost(2,5,row,-1)) || leftmost(2,5,row,1) == 0)
            ids = ids - [1 0];
        end
        if ((rightmost(2,5,row,1)>rightmost(2,5,row,-1)) || rightmost(2,5,row,-1) == 0)
            ids = ids + [0 1];
        end
        %leftmost inwards pointing crossing points (towards feasible region)
        posL = leftmost(ids(1,1),ids(1,2),row,1);
        posL2 = leftmost(posL+1,ids(1,2),row,1);
        posL3 = leftmost(posL2+1,ids(1,2),row,1);
    
        if(posL2 -posL ==1 && posL3-posL2 == 1)
            posL2 = 0;
            posL3 = 0;
        end
        
        if(posL2 - posL == 1) 
            posL2 = 0;
        end

        if(posL3 - posL2 == 1)
            posL3 = 0;
        end

        POSL = [posL posL2 posL3 ids(1,2)];
        POSLN = []; %based on changing its size
        counter = 0;
        %every (in) crossing point ID stored in a vector  
        for i = 1:4
            if(POSL(1,i) ~= 0)
                counter = counter+1;
                POSLN(1,counter) = POSL(1,i);
            end
        end
        %for every (in) point, the rightmost (out) point is found 
        for i = 1:length(POSLN)-1
            %rightmost (out) point between the two (in) points
            negR = rightmost(POSLN(1,i),POSLN(1,i+1),row,-1);
            if(negR > 0)
                for k = i:3
                    %a pair of (in) and (out) is assembled
                    pair(j,2*k-1:2*k) = [POSLN(1,i) negR];
                    %corresponding Psi values assigned
                    intPair(j,2*k-1:2*k) = [Int(j,POSLN(1,i)) Int(j,negR)];
                end
            end
        end
    end
    %assempling the Psi limits pairs (adding th4 as the max span [-pi,pi])
    intPair = [intPair(1:3,:); [-3.14 3.14 -3.14 3.14 -3.14 3.14]; intPair(4:6,:)];
    %finding the interval ends
    [mx,idMx] = max(intPair(:,1));
    [mn,idMn] = min(intPair(:,2));
    [mx2,idMx2] = max(intPair(:,3));
    [mn2,idMn2] = min(intPair(:,4));
    [mx3,idMx3] = max(intPair(:,5));
    [mn3,idMn3] = min(intPair(:,6));   

    %used for printing   
    limL = [mx;mx2;mx3];
    limU = [mn;mn2;mn3]; 
    Limss = [limL [Psi;Psi;Psi] limU];
    %used for printing 
    THlimits = [THL(:,1) TH_currentNorm(:,1) THU(:,1)];

    %logic for selecting the intervals
    if(Psi < limU(1,1) && Psi > limL(1,1) && limU(1,1) > limL(1,1))
        m = 1;
    elseif(Psi < limU(2,1) && Psi > limL(2,1) && limU(2,1) > limL(2,1))
        m = 2;
    elseif(Psi < limU(3,1) && Psi > limL(3,1) && limU(3,1) > limL(3,1))
        m = 3;
    else %failure condition (could find a feasible interval)
        n = 4;
    end
    
        %repulsive Psi function parameters
        K = 0.3; %[0,1] controlls the strength of repulsion
        a = 0.1; %distance to the limits from where the arm angle starts to repulse
        
    if(limU(m,1)-limL(m,1) < 0.1) %for running out of feasible interval
            Psi = Psi;
    else %new arm angle is found based on repulsion from the interval limits
        Psin = Psi + K*((limU(m,1)-limL(m,1))/2)*(exp(-a*((Psi-limL(m,1))/(limU(m,1)-limL(m,1))))...
        -exp(-a*((limU(m,1)-Psi)/(limU(m,1)-limL(m,1)))));    
        Psi = Psin;
    end      
    

end


                        
%finding the leftmost value within the specified interval of the given row
function leftmost = leftmost(id,id2,row,val)
    for i = id:id2
        if (row(1,i)==val)
            leftmost = i;
            break
       
        else
            leftmost = 0;
         end
    end
end
%finding the rightmost value within the specified interval of the given row
function rightmost = rightmost(id,id2,row,val)
    for j = id:id2
        i = (id2-j)+id;
        if (row(1,i)==val)
            rightmost = i;
            break
       
        else
            rightmost = 0;
         end
    end
end
