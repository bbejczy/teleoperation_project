function FowKinPlotter(TH,state,view_angle)
    %function plotting the manipulator links based on the forward kin
    %calculation

    %TH = THab;
    T0 = eye(4);
    EEFp0 = [0 0 0]';

    dbs = 34/100; 
    dse = 40/100;
    dew = 40/100;
    dwf = 12.55/100;

    a1 = 0; a2 = 0; a3 = 0; a4 = 0; a5 = 0; a6 = 0; a7 = 0;
    d1 = dbs; d2 = 0; d3 = dse; d4 = 0; d5 = dew; d6 = 0; d7 = dwf;

    al1 = 0;
    al2 = -pi/2;
    al3 = pi/2;
    al4 = pi/2;
    al5 = -pi/2;
    al6 = -pi/2;
    al7 = pi/2;

    lengthTH = size(TH);
    for j=1:lengthTH(1,2)
        th1 = TH(1,j);
        th2 = TH(2,j);
        th3 = TH(3,j);
        th4 = TH(4,j);
        th5 = TH(5,j);
        th6 = TH(6,j);
        th7 = TH(7,j);

        R01 = [cos(th1) -sin(th1) 0;
        sin(th1)*cos(al1) cos(th1)*cos(al1) -sin(al1);
        sin(th1)*sin(al1) cos(th1)*sin(al1) cos(al1)];

        P01 = [a1; 
        -d1*sin(al1);
        d1*cos(al1)];

        T1 = [R01 P01; 0 0 0 1];
        T01 = T0 * T1;
        EEFp = [T01(1,4) T01(2,4) T01(3,4)]';

        figure(1)
        if(state==0)
        clf
        end
        line1=line([EEFp0(1,1) EEFp(1,1)],[EEFp0(2,1) EEFp(2,1)],[EEFp0(3,1) EEFp(3,1)],'LineWidth',1.7, 'color', [0.8500, 0.3250, 0.0980]);
        aa = double(50+view_angle(1,1)+view_angle(2,1));
        view(aa,50)
        axis([-0.6 1.1 -0.6 1.1 -0.1 1.5]);
        hold on
        
        EEFp0 = EEFp;
        T0 = T01;

        R12 = [cos(th2) -sin(th2) 0;
        sin(th2)*cos(al2) cos(th2)*cos(al2) -sin(al2);
        sin(th2)*sin(al2) cos(th2)*sin(al2) cos(al2)];

        P12 = [a2; 
        -d2*sin(al2);
        d2*cos(al2)];

        T2 = [R12 P12; 0 0 0 1];
        T12 = T0 * T2;
        EEFp = [T12(1,4) T12(2,4) T12(3,4)]';
        line2=line([EEFp0(1,1) EEFp(1,1)],[EEFp0(2,1) EEFp(2,1)],[EEFp0(3,1) EEFp(3,1)],'LineWidth',1.7, 'color', [0.8500, 0.3250, 0.0980]);
        %plot3(EEFp(1,1),EEFp(2,1), EEFp(3,1),'ko');
        EEFp0 = EEFp;
        T0 = T12;   

        R23 = [cos(th3) -sin(th3) 0;
        sin(th3)*cos(al3) cos(th3)*cos(al3) -sin(al3);
        sin(th3)*sin(al3) cos(th3)*sin(al3) cos(al3)];

        P23 = [a3; 
        -d3*sin(al3);
        d3*cos(al3)];

        T3 = [R23 P23; 0 0 0 1];
        T23 = T0 * T3;
        EEFp = [T23(1,4) T23(2,4) T23(3,4)]';
        line3=line([EEFp0(1,1) EEFp(1,1)],[EEFp0(2,1) EEFp(2,1)],[EEFp0(3,1) EEFp(3,1)],'LineWidth',1.7, 'color', [0.8500, 0.3250, 0.0980]);
        %plot3(EEFp(1,1),EEFp(2,1), EEFp(3,1),'ko');
        EEFp0 = EEFp;
        T0 = T23;

        R34 = [cos(th4) -sin(th4) 0;
        sin(th4)*cos(al4) cos(th4)*cos(al4) -sin(al4);
        sin(th4)*sin(al4) cos(th4)*sin(al4) cos(al4)];

        P34 = [a4; 
        -d4*sin(al4);
        d4*cos(al4)];

        T4 = [R34 P34; 0 0 0 1];
        T34 = T0 * T4;
        EEFp = [T34(1,4) T34(2,4) T34(3,4)]';
        line4=line([EEFp0(1,1) EEFp(1,1)],[EEFp0(2,1) EEFp(2,1)],[EEFp0(3,1) EEFp(3,1)],'LineWidth',1.7, 'color', [0.8500, 0.3250, 0.0980]);
        %plot3(EEFp(1,1),EEFp(2,1), EEFp(3,1),'ko');
        EEFp0 = EEFp;
        T0 = T34;

        R45 = [cos(th5) -sin(th5) 0;
        sin(th5)*cos(al5) cos(th5)*cos(al5) -sin(al5);
        sin(th5)*sin(al5) cos(th5)*sin(al5) cos(al5)];

        P45 = [a5; 
        -d5*sin(al5);
        d5*cos(al5)];

        T5 = [R45 P45; 0 0 0 1];
        T45 = T0 * T5;
        EEFp = [T45(1,4) T45(2,4) T45(3,4)]';
        line5=line([EEFp0(1,1) EEFp(1,1)],[EEFp0(2,1) EEFp(2,1)],[EEFp0(3,1) EEFp(3,1)],'LineWidth',1.7, 'color', [0.8500, 0.3250, 0.0980]);
        %plot3(EEFp(1,1),EEFp(2,1), EEFp(3,1),'ko');
        EEFp0 = EEFp;
        T0 = T45;

        R56 = [cos(th6) -sin(th6) 0;
        sin(th6)*cos(al6) cos(th6)*cos(al6) -sin(al6);
        sin(th6)*sin(al6) cos(th6)*sin(al6) cos(al6)];

        P56 = [a6; 
        -d6*sin(al6);
        d6*cos(al6)];

        T6 = [R56 P56; 0 0 0 1];
        T56 = T0 * T6;
        EEFp = [T56(1,4) T56(2,4) T56(3,4)]';
        line6=line([EEFp0(1,1) EEFp(1,1)],[EEFp0(2,1) EEFp(2,1)],[EEFp0(3,1) EEFp(3,1)],'LineWidth',1.7, 'color', [0.8500, 0.3250, 0.0980]);
        %plot3(EEFp(1,1),EEFp(2,1), EEFp(3,1),'ko');
        EEFp0 = EEFp;
        T0 = T56;

        R67 = [cos(th7) -sin(th7) 0;
        sin(th7)*cos(al7) cos(th7)*cos(al7) -sin(al7);
        sin(th7)*sin(al7) cos(th7)*sin(al7) cos(al7)];

        P67 = [a7; 
        -d7*sin(al7);
        d7*cos(al7)];

        T7 = [R67 P67; 0 0 0 1];
        T67 = T0 * T7;
        EEFp = [T67(1,4) T67(2,4) T67(3,4)]';
        line7=line([EEFp0(1,1) EEFp(1,1)],[EEFp0(2,1) EEFp(2,1)],[EEFp0(3,1) EEFp(3,1)],'LineWidth',1.7, 'color', [0.5500, 0.1250, 0.5980]);
        %line([EEFp0(1,1) EEFp(1,1)],[EEFp0(2,1) EEFp(2,1)],[EEFp0(3,1) EEFp(3,1)],'LineWidth',1.7, 'color', [0.5500, 0.1250, 0.5980]);
   
        %plot3(EEFp(1,1),EEFp(2,1), EEFp(3,1),'ko');
        EEFp0 = [0 0 0]';
        T0 = eye(4);

              

        axisX = line([-0.5 0.5],[-0.5 -0.5],[0 0],'LineWidth',1.2, 'color','r');
        axisY = line([-0.5 -0.5],[-0.5 0.5],[0 0],'LineWidth',1.2, 'color','g');
        axisZ = line([-0.5 -0.5],[-0.5 -0.5],[0 1],'LineWidth',1.2, 'color','b');


    end
end