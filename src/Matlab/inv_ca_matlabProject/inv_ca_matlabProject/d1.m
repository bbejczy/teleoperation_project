function d1 = d1(k,J,p)
    L = norm(J(:,k+1) - J(:,k));
    R1 = norm(J(:,k) -p);
    R2 = norm(J(:,k+1) -p);

    d1 = (R1+R2)/L; % ratio of elipsoid 

    
end
