function [R07,J] = fowKin(TH)
    % function of R07 = R01*R12*...*R67

    th1 = TH(1,1);
    th2 = TH(2,1);
    th3 = TH(3,1);
    th4 = TH(4,1);
    th5 = TH(5,1);
    th6 = TH(6,1);
    th7 = TH(7,1);
    T0 = eye(4);
    J0 = transpose([0 0 0]);

    dbs = 34/100; 
    dse = 40/100;
    dew = 40/100;
    dwf = 12.55/100;

    a1 = 0; a2 = 0; a3 = 0; a4 = 0; a5 = 0; a6 = 0; a7 = 0;
    d1 = dbs; d2 = 0; d3 = dse; d4 = 0; d5 = dew; d6 = 0; d7 = dwf;

    al1 = 0;
    al2 = -pi/2;
    al3 = pi/2;
    al4 = pi/2;
    al5 = -pi/2;
    al6 = -pi/2;
    al7 = pi/2;


        R01 = [cos(th1) -sin(th1) 0;
        sin(th1)*1 cos(th1)*1 0;
        sin(th1)*0 cos(th1)*0 1];

        P01 = [a1; 
        -d1*sin(al1);
        d1*cos(al1)];

        T1 = [R01 P01; 0 0 0 1];
        T01 = T0 * T1;
        J1 = transpose([T01(1,4) T01(2,4) T01(3,4)]);
        %J0 = J1;
        T0 = T01;

        R12 = [cos(th2) -sin(th2) 0;
        sin(th2)*0 cos(th2)*0 1;
        sin(th2)*(-1) cos(th2)*(-1) 0];

        P12 = [a2; 
        -d2*sin(al2);
        d2*cos(al2)];

        T2 = [R12 P12; 0 0 0 1];
        T02 = T0 * T2;
        J2 = transpose([T02(1,4) T02(2,4) T02(3,4)]);
       %J0 = J2;
        T0 = T02  ;


        R23 = [cos(th3) -sin(th3) 0;
        sin(th3)*0 cos(th3)*0 -1;
        sin(th3)*1 cos(th3)*1 0];

        P23 = [a3; 
        -d3*sin(al3);
        d3*cos(al3)];

        T3 = [R23 P23; 0 0 0 1];
        T03 = T0 * T3;
        J3 = transpose([T03(1,4) T03(2,4) T03(3,4)]);
        %J0 = J3;
        T0 = T03;

        R34 = [cos(th4) -sin(th4) 0;
        sin(th4)*0 cos(th4)*0 -1;
        sin(th4)*1 cos(th4)*1 0];

        P34 = [a4; 
        -d4*sin(al4);
        d4*cos(al4)];

        T4 = [R34 P34; 0 0 0 1];
        T04 = T0 * T4;
        J4 = transpose([T04(1,4) T04(2,4) T04(3,4)]);
        %J0 = J4;
        T0 = T04;

        R45 = [cos(th5) -sin(th5) 0;
        sin(th5)*0 cos(th5)*0 1;
        sin(th5)*-1 cos(th5)*-1 0];

        P45 = [a5; 
        -d5*sin(al5);
        d5*cos(al5)];

        T5 = [R45 P45; 0 0 0 1];
        T05 = T0 * T5;
        J5 = transpose([T05(1,4) T05(2,4) T05(3,4)]);
        %J0 = J5;
        T0 = T05;

        R56 = [cos(th6) -sin(th6) 0;
        sin(th6)*0 cos(th6)*0 1;
        sin(th6)*-1 cos(th6)*-1 0];

        P56 = [a6; 
        -d6*sin(al6);
        d6*cos(al6)];

        T6 = [R56 P56; 0 0 0 1];
        T06 = T0 * T6;
        J6 = transpose([T06(1,4) T06(2,4) T06(3,4)]);
        %J0 = J6;
        T0 = T06;

        R67 = [cos(th7) -sin(th7) 0;
        sin(th7)*0 cos(th7)*0 -1;
        sin(th7)*1 cos(th7)*1 0];

        P67 = [a7; 
        -d7*sin(al7);
        d7*cos(al7)];

        T7 = [R67 P67; 0 0 0 1];
        T07 = T0 * T7;
        J7 = transpose([T07(1,4) T07(2,4) T07(3,4)]);
        %J0 = transpose([0 0 0]);
        T0 = eye(4);
        
        R07 = T07(1:3,1:3);
        J = [J1 J2 J3 J4 J5 J6 J7];
        
end
        