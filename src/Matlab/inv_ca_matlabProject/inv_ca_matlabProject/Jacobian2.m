function Jacobian2 = Jacobian2(Ji,aID,bID,THi,d1i,d2i,k,p,qi)
   
    %different Jacobian scaling (defined by calibration (sensitivity))
    if (k == 6)
         THchange = 0.0002;
         Kj = 5;
    elseif (k == 4)
        THchange = 0.002;
        Kj = 2;
    elseif (k == 2)
        THchange = 0.02; 
        Kj = 2;
        
    end

    %numeric differentiation for tha and thb
    THdiffa=THi;
    THdiffa(aID,1)=qi(1,1)+THchange;
    
    THdiffb=THi;
    THdiffb(bID,1)=qi(2,1)+THchange;
   
    [R07_diffa,Jdiffa] = fowKin(THdiffa);
    d1diffa = d1(k,Jdiffa,p);
    d2diffa = d2(k,Jdiffa,Ji);
    
    [r07_diffb,Jdiffb] = fowKin(THdiffb);
    d1diffb = d1(k,Jdiffb,p);
    d2diffb = d2(k,Jdiffb,Ji);
    
    %reduced Jacobian for link2 and link4
    if (k == 2 || k == 4)
       Jacobian2 = [d1diffa-d1i 0; 0 1]/THchange*Kj;

    else
        Jacobian2 = [d1diffa-d1i d2diffa-d2i; d1diffb-d1i d2diffb-d2i]/THchange*Kj;
    end
    
    
end