# Teleoperation Project for Novo Nordisk
## Aalborg University ROB6 G669

### Report title

Control and interface improvements for a teleoperation solution in cleanroom applications

### Abstract

This repository contains the source code for the corresponding report investigating control and interfacing improvements to a 7 degrees-of-freedom manipulator teleoperation system, employing a mixed reality interface for small object manipulation applications, in the context of Novo Nordisk's cleanroom filling line environment. Expansions to the system are introduced in the areas of inverse kinematics, collision avoidance, trajectory generation and keyword spotting. The feasibility of the expansions is shown through individual tests and demonstrations, followed by a combined demonstration of the integrated system.

### Important repositories

- The matlab code for **inverse kinematics, collision avoidance** and **semi-autonomy** can be found in src/Matlab.
- The **unity project** can be found in src/unity and the **keyword spotter** can be found in src/keyword_spotter.
- The additional ROS packages can be found in the ros_ws folder.

### Demonstrations videos

- Semi-autonomy: https://youtu.be/yY6hRQxuJOw
- Collision avoidance and inverse kinematics: https://youtu.be/IIGWDzg7M1g
- Final demonstration of the combined system: https://youtu.be/Zm4xJxx0N3w

#### Sources
- Documentation on moveit and source: https://moveit.ros.org/
- Iiwa_stack original source: https://github.com/IFL-CAMP/iiwa_stack
